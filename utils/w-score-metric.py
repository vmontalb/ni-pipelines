#!/usr/bin/env python
#!/bin/sh
#w-score-metric.py

##############################
#
# This script computes the W-score value for a given metric (Cth, MD, whatevah), following indications in La Joie 2012
#
#############################

# ==========================
# Import Libs
import os , pickle, time, sys
import argparse, logging, subprocess

import numpy as np
import scipy.io as sp
import nibabel.freesurfer.mghformat as nibmgh
import nibabel.freesurfer.io as nib
import scipy.sparse.csgraph as csgraph

import pdb


# ---------------------------
# Parser Options
HELPTEXT = """

w-score-metric.py

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat


>>  <<


REFS:
-----

[1]



"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_txtfile = 'A .txt with subjects (FS) name && Age && SEx of each subjects file (each in one column).'
    h_betaintercept = 'Beta-intercept from the model.'
    h_betage = 'Beta-age from the model.'
    h_betsex = 'Beta-sex from the model.'
    h_std = 'SD from the residuals of the model.'
    h_metric = 'Metric name'
    h_fwhm = 'Smoothing of the resulting map (e.g 5, 10, 15)'
    h_fspath = 'Path to Freesurfer processed subjects.'
    h_hemi = 'Hemisphere'

    parser.add_argument('--glm-file',       dest = 'glmfile',   action = 'store',       help = h_txtfile,           required = True)
    parser.add_argument('--b-intercept',    dest = 'binter',    action = 'store',       help = h_betaintercept,     required = True)
    parser.add_argument('--b-age',          dest = 'bage',      action = 'store',       help = h_betage,            required = True)
    parser.add_argument('--b-sex',          dest = 'bsex',      action = 'store',       help = h_betsex,            required = True)
    parser.add_argument('--std',            dest = 'stdfile',   action = 'store',       help = h_std,               required = True)
    parser.add_argument('--metric',         dest = 'metric',    action = 'store',       help = h_metric,            required = True)
    parser.add_argument('--fwhm',           dest = 'fwhm',      action = 'store',       help = h_fwhm,              required = True)
    parser.add_argument('--fspath',         dest = 'fspath',    action = 'store',       help = h_fspath,            required = True)
    parser.add_argument('--hemi',           dest = 'hemi',      action = 'store',       help = h_hemi,              required = True)


    args = parser.parse_args()

    return args


# ---------------------------
# Subfunctions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

def run_cmd(cmd,err_msg):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n')
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg)
        sys.exit(1)
    my_print('\n')


# ---------------------------
# Main Code
def Wscore(options):

    # Read inputs
    intercept = nibmgh.load(options.binter)
    intercept = intercept.get_data()
    intershape = intercept.shape
    rsintercept = intercept.reshape(intershape[0], intershape[1] * intershape[2])

    bage = nibmgh.load(options.bage)
    bage = bage.get_data()
    bageshape = bage.shape
    rsbage = bage.reshape(bageshape[0], bageshape[1] * bageshape[2] )

    bsex = nibmgh.load(options.bsex)
    bsex = bsex.get_data()
    bsexshape = bsex.shape
    rsbsex = bsex.reshape(bsexshape[0], bsexshape[1] * bsexshape[2] )

    stdresid = nibmgh.load(options.stdfile)
    stresid = stdresid.get_data()
    stresidshape = stresid.shape
    rsstd = stresid.reshape(stresidshape[0], stresidshape[1] * stresidshape[2])

    # For eacj file
    glmfile = np.loadtxt(options.glmfile, dtype=np.object)
    subjlist = glmfile[:,0]
    agelist = glmfile[:,1].astype(np.float)
    sexlist = glmfile[:,2].astype(np.float)
    for idx,subject in enumerate(subjlist):
        my_print('Working with subject: '+str(subject))
        # Read input
        fmetric = os.path.join(options.fspath,subject,'surf',options.hemi+'.'+options.metric+'.fwhm15.fsaverage.mgh')
        infile = nibmgh.load(fmetric)
        infile = infile.get_data()
        infileshape = infile.shape
        rsinfile = infile.reshape(infileshape[0], infileshape[1] * infileshape[2])
        # Check for vertex with non-zero values
        mask = np.where(rsinfile > 0 )[0]
        # Normalize (Wscore)
        norm = np.zeros_like(rsinfile)
        numer = np.zeros_like(rsinfile)
        numer[mask] = rsinfile[mask] - (np.multiply(rsbage[mask],agelist[idx]) + rsintercept[mask] + np.multiply(rsbsex[mask] ,sexlist[idx]))
        norm[mask] = np.divide( numer[mask], rsstd[mask])
        norm = np.nan_to_num(norm)

        # Save
        fname = options.hemi+'.Wscore-'+options.metric+'.fwhm15.fsaverage.mgh'
        outfile = os.path.join(options.fspath,subject,'surf', 'tmp.sulc')
        nib.write_morph_data(outfile, norm[:, np.newaxis])
        cmd = 'mri_convert '+outfile+' '+os.path.join(options.fspath,subject,'surf',fname)
        run_cmd(cmd, 'Error converting wscore file from .sulc to .mgh')
        cmd = 'mri_binarize --i '+os.path.join(options.fspath,subject,'surf',fname)+' --o '+os.path.join(options.fspath,subject,'surf','mask.mgh')+' --max 45'
        run_cmd(cmd, 'Error generating mask for the outlayers (due to missregistrations)')
        cmd = 'mris_calc -o '+os.path.join(options.fspath,subject,'surf',fname)+' \
                        '+os.path.join(options.fspath,subject,'surf',fname)+' mul '+os.path.join(options.fspath,subject,'surf','mask.mgh')
        run_cmd(cmd, 'Error masking outlayers')
        # remove useless files
        os.remove(outfile)
        os.remove(os.path.join(options.fspath,subject,'surf','mask.mgh'))


# ---------------------------
# Run Code
if __name__ == "__main__":
    options = options_parser()
    Wscore(options)
