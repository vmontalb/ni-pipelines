#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Fri Oct  1 17:56:51 2020

@Original author: pegue
@Modify author: vmontalb
'''

# ---------------------------
# Import Libraries
import os
import sys
from os.path import join
import argparse
import logging
import subprocess
import shutil as sh
import json

import pandas as pd
import numpy as np

from pathlib import Path

import pdb

# ---------------------------
# Parser Options
HELPTEXT = '''
 > Script that will parse ADNI DICOM data (in a folder) to a BIDS format <

 Priors:
 -------
 · All the working on THIS script, is based to have a CLEAN dataframe of ADNI info.
 Such dataframe will contain, per each subject, per each visit, the image ID.

 · This is a MUST. Our data-organization will mirror the ADNI-repo data organization
 (i.e if one subject have image_1 in visit3, we will put image_1 in visit3, too)

 · Thus, we will NOT consider date info from ADNI dicoms to create the BIDS Folder
 organization.


 INPUTS:
 -------
  dicomdir   : string
                Full-path where the downloaded ADNI dicoms are placed

  adni-df    : string
                Full-path where the data ADNI dataframe is placed

  bidsdir    : string
                Full-path where the converted data in BIDS format will be saved

 WORKING DIAGRAM:
 ----------------
 1) Loop over Dicom folders (i.e each individual)
 2) Detect modality (by folder name or imageID)
 3) For each modality, detect imageID (by folder name)
 4) Get visit number, based on imageID, from adni-dataframe
 5) Create BIDS folder (if does not exist)
 6) Convert dcm to nii and store .json metadata
 7) Rename data (if needed)

 . FUTURE TO DO .
 8) Do sanity check
  8.1) From all data in the BIDs folder, check if it exist in the dataframe
  8.2) Save missmatching data.

'''

USAGE = '''
'''

def options_parser():
    '''
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    '''

    parser = argparse.ArgumentParser(description = "", usage = HELPTEXT)

    # help text
    h_dicomdir =  'Full-path where the downloaded ADNI dicoms are placed'
    h_bidsdir  =  'Full-path where the converted data in BIDS format will be saved'
    h_adnidf   = 'Full-path where the data ADNI dataframe is placed'

    parser.add_argument('--dicomdir',   dest = 'top_dicom_dir', action = 'store', help = h_dicomdir,    required = True)
    parser.add_argument('--bidsdir',    dest = 'top_bids_dir',  action = 'store', help = h_bidsdir,     required = True)
    parser.add_argument('--adni-df',    dest = 'adnidf',        action = 'store', help = h_adnidf,      required = True)
    args = parser.parse_args()

    # Check options (toDo)
    return args

# ---------------------------
# Subfunction
def add_field_json(jsonfile, field, value):
    with open(jsonfile) as infile:
        data=json.load(infile)
        data[field] = value
    with open(jsonfile, 'w') as outfile:
        json.dump(data, outfile,  indent='\t')

def my_print(message, ff):
    """
    print message, then flush stdout
    """
    print(message)
    ff.write(message+"\n")
    sys.stdout.flush()

def run_cmd(cmd,err_msg,ff):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)

# ---------------------------
# Main Code
def adni2bids(options):
    """
    0)  Load adni df
    1)  For each subject-folder.
    2)  Subset adni-df by subject name.
    3)  Loop over all folders
    4)  Go two levels deep.
    5)  Get image ID.
    6)  Search in subset df, for column(modality) where imageID is stored
    7)  Convert columnName to imageModality
    8)  Get visit name of current modality/image
      8.1) Re-name baseline to m00
    9)  Check folder (visit-folder) exist (and create it)
    10) Create imageModality folder (BIDS format)
    11) Convert dcm data to .nii inside BIDS folder
    12) Add acquisitionDate and imageID into .json file
    13) If T1, run N4 FieldCorrection
    14) Create correspondant derivative folders

    Dicom schema:

    subject
        | - modality1
                | - Date 1
                       | - Dicoms
        | - modality2
                | - Date 1
                       | - Dicoms
                | - Date 2
                       | - Dicoms
    """
    # Priors
    convmod = {'T1-id' : 'T1w',
                'DWI-uniband-id' : 'dwi',
                'DWI-multiband-id' : 'dwi',
                'AV1451-id' : 'AV1451',
                'AV45-id' : 'AV45',
                'FBB-id' : 'FBB',
                'PIB-id' : 'PIB',
                'fMRI-id' : 'task-rest_bold'}

    convdtype = {'T1-id' : 'anat',
                'DWI-uniband-id' : 'dwi',
                'DWI-multiband-id' : 'dwi',
                'AV1451-id' : 'pet',
                'AV45-id' : 'pet',
                'FBB-id' : 'pet',
                'PIB-id' : 'pet',
                'fMRI-id' : 'func'}

    errfile = join(options.top_bids_dir,'error_image_path.txt')
    if os.path.isfile(errfile):
        os.remove(errfile)   # Clean previous errors

    logf = join(options.top_bids_dir,'log_adni2bids.txt')
    if os.path.isfile(logf):
        os.remove(logf)   # Clean previous log
    logfile = open(logf, 'w')

    # Load adni dataframe
    my_print('> Loading ADNI dataframe file', logfile)
    my_print('.     File path: '+options.adnidf, logfile)
    adf = pd.read_csv(options.adnidf, index_col = 'MIX-id')
    adf.fillna('no-data', inplace=True)

    # Check main BIDS folder exist
    if not os.path.isdir(join(options.top_bids_dir, 'sourcedata')):
        my_print('> Creating BIDS output folder', logfile)
        my_print('.     Folder path: '+options.top_bids_dir, logfile)
        os.makedirs(join(options.top_bids_dir, 'sourcedata'))
        os.makedirs(join(options.top_bids_dir, 'derivatives'))

    # Loop over subjects
    p = Path(options.top_dicom_dir)
    subjects = [os.path.basename(x) for x in p.iterdir() if x.is_dir()]

    for csubj in subjects:
        ## Subset adni-df
        subadf = adf[adf['PTID'] == csubj]
        ## Subject priors
        csubjbids = csubj.replace('_','')
        csubjbids = 'sub-'+ csubjbids
        sbids = join(options.top_bids_dir, 'sourcedata',csubjbids)
        dbids = join(options.top_bids_dir, 'derivatives',csubjbids)
        my_print('> Working with subject: '+csubjbids, logfile)

        ## Loop over all subfolder / images
        csubjdcm = join(options.top_dicom_dir,csubj)
        p = Path(csubjdcm)
        modalities = [x for x in p.iterdir() if x.is_dir()]

        for cmod in modalities:
            csubjmod = join(csubjdcm,cmod)
            p = Path(csubjmod)
            dates = [x for x in p.iterdir() if x.is_dir()]

            ## Loop over all dates for each modality
            for cdate in dates:
                csubjdate = join(csubjmod,cdate)
                p = Path(csubjdate)
                ## Get imageID and its column in dataframe
                images = [x for x in p.iterdir() if x.is_dir()]
                if len(images) > 1:
                    pdb.set_trace()
                    my_print('.     Warning. More than one imageID found', logfile)
                    my_print('.              Chech '+errfile, logfile)
                    with open(errfile,'a') as f:
                        f.write("Multi-image id: "+csubjmod +'\n')
                    continue
                imgid = os.path.basename(images[0])
                imgfullpath = str(images[0])

                ## Get imageID modality
                tmpmod = (subadf == imgid).any().idxmax()
                if tmpmod == "RID":
                    # imageID might (or not) start with I
                    if imgid.startswith("I"):
                        imgid = imgid[1:]      # delete starting I and search again
                    else:
                        imageid = "I"+imgid   # add starting I and search again
                    tmpmod = (subadf == imgid).any().idxmax()
                imgmod = convmod[tmpmod]    # translate image modality
                imgtype = convdtype[tmpmod]    # translate image data-type

                ## Get imageID visit
                visitdf = subadf[subadf[tmpmod].str.contains(imgid)]  # Get visit row
                cvisit = visitdf['VISCODE'].values.astype(str)[0]  # Avoid narray access
                if cvisit == "bl":
                    cvisit = "m00"
                cvisit = 'ses-'+cvisit

                ## Check if modality-visit folder exist or create
                cbidsfolder = join(sbids, cvisit, imgtype)
                if not os.path.isdir(cbidsfolder):
                    os.makedirs(cbidsfolder)

                ## Convert dcm to .nii
                outname = join(csubjbids+"_"+cvisit+"_"+imgmod)
                jsonfile = join(cbidsfolder, outname+".json")
                my_print('.     File: '+outname, logfile)
                if not os.path.isfile(join(cbidsfolder, outname+".nii")):
                    cmd = 'dcm2niix -b y -z n -f %s -o %s %s' % \
                      (outname, cbidsfolder, imgfullpath)
                    try:
                        subprocess.run(cmd, shell=True, stderr=subprocess.DEVNULL,
                                        stdout=subprocess.DEVNULL, check=True)
                        ## Add AcqDate and imageID to .json folder
                        acqdate = os.path.basename(str(cdate)).split("_")[0]   # get date (skip time)
                        add_field_json(jsonfile,'AcquisitionDate',acqdate)
                        add_field_json(jsonfile,'ImageID',imgid)

                    except:
                        my_print('.     Error. Could not convert dicom images', logfile)
                        with open(errfile,'a') as f:
                            f.write("Dicom conversion: "+cmd +'\n')
                        continue

                ## Run N4 ants correction to T1
                if imgmod == "T1w":
                    sh.copy(join(cbidsfolder, outname+".nii"),
                            join(cbidsfolder, outname+"orig.nii"))
                    sh.copy(join(cbidsfolder, outname+".json"),
                            join(cbidsfolder, outname+"orig.json"))

                    cmd = 'N4BiasFieldCorrection -i %s -o %s -s 2 -d 3' % \
                               (join(cbidsfolder, outname+"orig.nii"),
                                join(cbidsfolder, outname+".nii"))
                    torun = 'samrun -j N4-adni2bids -c "'+cmd+'"'
                    subprocess.run(torun, shell=True, stderr=subprocess.DEVNULL,
                                    stdout=subprocess.DEVNULL, check=True)
                    add_field_json(jsonfile,'Preprocessing','N4corrected')

                ## Create derivatives folders
                cbidsderivate = join(dbids, cvisit, imgtype)
                if not os.path.isdir(cbidsderivate):
                    os.makedirs(cbidsderivate)





# ---------------------------
# Run
if __name__ == "__main__":
    options = options_parser()
    adni2bids(options)

