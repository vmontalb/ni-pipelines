#!/usr/bin/env python
#!/bin/sh
#altas86_to_FSLUT.py

##############################
#
# This script computes a dictionary conversion between the atlas96 from NeMo Tools to
# the FreeSurfer color LUT .
#  (i.e number 1 in atlas86  (Bankssts_R) matrix means number )
#
#
#############################

# ==========================
# Import Libs
import os , pickle, time
import argparse, logging, subprocess, coloredlogs

import numpy as np
import pdb


# ==========================
# Inputs
version = '0.0.1'
#logging.basicConfig(format='%(asctime)s || %(name)s >> %(levelname)s : %(message)s ', datefmt='%I:%M:%S %p')
os.environ['COLOREDLOGS_LOG_FORMAT'] = '%(asctime)s || %(name)s >> %(levelname)s : %(message)s '
os.environ['COLOREDLOGS_DATE_FORMAT'] = '%I:%M:%S %p'
coloredlogs.install(level='DEBUG')
logger = logging.getLogger('atlas-conv')

# - Input variables.
description = """
Description:
 > This script computes a dictionary conversion between the atlas96 from NeMo Tools to the FreeSurfer color LUT. <

Inputs:
-------
  -o subjDir : path/to/dir with the NifTi files that will be used for the One-Sample T-test.

  -f outputPath : path/to/dir where stats results will be saved

Outputs:
--------



Example:
`basename $0` -d path/to/subjDir -o path/to/outputDir

---------------------------------
V.M.

Memory Unit, Department of Neurology,
Hospital de la Santa Creu i Sant Pau
Biomedical Research Institute Sant Pau
Universitat Autonoma de Barcelona

--------------------------------


"""
usage = ''
parser = argparse.ArgumentParser(description = description,usage = usage )

parser.add_argument('-o','--nemo',action = 'store', dest = 'nemolut', help = "Path to NeMo 86 parcellation atlas file", required = True)
parser.add_argument('-f','--lmax',action = 'store',  dest = 'fslut', help = 'Path to FreeSurfer cortical LUT', required = True)
args = parser.parse_args()


# ==========================
# Read inputs
logger.info("STARTING TO GENERATE THE CONVERSION DICT")
logger.info("Loading NeMo tools atlas-name table ...")
try:
        nemoatlas = np.loadtxt(args.nemolut, delimiter='=', dtype = np.object)
        logger.debug('NeMo atlas read correctly')

except (SystemExit, KeyboardInterrupt):
    raise
except Exception, e:
    logger.error('Failed to open NeMo atlas-name .cod file. May not exist', exec_info=True )

logger.info("Loading FreeSurfer LUT ...")
try:
        fsatlas  = np.loadtxt(args.fslut, dtype = np.object)
        logger.debug('FS atlas read correctly')

except (SystemExit, KeyboardInterrupt):
    raise
except Exception, e:
    logger.error('Failed to FreeSurfer LUT file. May not exist', exec_info=True )


# ==========================
# Main Code
logger.info("Creating dictionary ...")
logger.info("   ** This will use the NeMo number/label as INPUT name (i.e key) and the FreeSurfer LUT label/number as the value **")

conv = {}
for idx,label in enumerate(nemoatlas[:,1]):

    logger.info("   Working with label: "+str(label))
    label = label.lower()
    labelname = label.split('_')[0]
    labelhemi = label.split('_')[1]

    pos = [ x for x , item in enumerate(fsatlas[:,1]) if labelname in item ]
    logger.info("       Labels found in FS LUT: "+str(fsatlas[pos,1]))

    try:
        conv[int(nemoatlas[idx,0])] = int(fsatlas[pos,0])
    except Exception, e:
        logger.warning("Label "+str(label)+" does not have FreeSurfer LUT correspondance.")
        time.sleep(1)
        conv[int(nemoatlas[idx,0])] = None


pdb.set_trace()
with open("conversion_dict.pkl", 'wb') as f:
    pickle.dump(conv,f)

logger.info('------')
logger.info(' DONE ')
logger.info('------')
