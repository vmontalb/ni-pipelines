#!/usr/bin/env python
# eigenmodes_raj.py

#
# script to parse all eigenvalues from a given structure for a list of subjects in a single .txt file
#
# Original Author: Victor Montal
# Date_first: May-6-2017
# Date:


# ==========================
# Import Libs
import os , pickle, time
import argparse, logging, subprocess, coloredlogs

import numpy as np
import scipy.io as sp
import nibabel.freesurfer.mghformat as nibmgh
import nibabel.freesurfer.io as nib
import scipy.sparse.csgraph as csgraph

import pdb

# ==========================
# Inputs
version = '0.0.1'
#logging.basicConfig(format='%(asctime)s || %(name)s >> %(levelname)s : %(message)s ', datefmt='%I:%M:%S %p')
os.environ['COLOREDLOGS_LOG_FORMAT'] = '%(asctime)s || %(name)s >> %(levelname)s : %(message)s '
os.environ['COLOREDLOGS_DATE_FORMAT'] = '%I:%M:%S %p'
coloredlogs.install(level='DEBUG')
logger = logging.getLogger('non-local eff')

# ==========================
# Main code
logger.info("Starting to compute non-local effects ...")

# - Load dict_conv and revers (i.e from )
logger.info("   Loading conversion dictionary: from NeMo to Freesurfer LUT ")
with open('/home/neuroimagen2/Documents/scripts/Diffusion-Network/conversion_dict.pkl', 'rb') as f:
    dickt =  pickle.load(f)
idickt = [dict((v,k) for k,v in dickt.iteritems()) ][0]

# - Load Connectivity matrix and normalize
logger.info("   Loading Diffusion Network Matrix and normalizing it ... ")
mat = sp.loadmat('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Tools/NeMo-master/Tractograms/FiberTracts86_MNI_BIN/TRS_matrix_old.mat')
connmat = mat['fTRS']
sumconn = np.sum(connmat, axis = 1)
conn = np.divide(connmat, sumconn[:,np.newaxis] )

# - Load aparc+aseg fsaverage .annot file
logger.info("   Loading ?h.aparc.annot files ")
[labelslh, ctablh, nameslh] = nib.read_annot('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/lh.aparc.annot')
[labelsrh, ctabrh, namesrsh] = nib.read_annot('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/rh.aparc.annot')
annotlabels = np.append(np.add(labelslh,1000), np.add(labelsrh,2000))


# - Load aparc+aseg fsaverage .annot file
logger.info("   Loading ?h.aparc.annot files ")
[labelslh, ctablh, nameslh] = nib.read_annot('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/lh.aparc.annot')
[labelsrh, ctabrh, namesrsh] = nib.read_annot('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/rh.aparc.annot')
annotlabels = np.append(np.add(labelslh,1000), np.add(labelsrh,2000))

# - Convertin aparc.annot labels to NeMo labels
logger.info("   Converting aparc to NeMo labels  ")
nemolabels = np.multiply(annotlabels, 0)
for idxs,value in enumerate(annotlabels):
    try:
        nemolabels[idxs] = idickt[value]
    except Exception, e:
        nemolabels[idxs] = -1

# - Compute the laplacian















pass
