function [ ok ] = single_heat_diffusion_kernel( corr_matrix, maskname, ROIfile )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some init vairiables..
[~,cname,ext]=fileparts(corr_matrix);
[~,roiname,~] = fileparts(ROIfile);
pathtosaveind = '/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Heat_diffusion_kernel/output/';
pathtosavecorr = '/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Heat_diffusion_kernel/Corr_matrix/';

pr = ['Starting SFC analysis of subject: ' cname '\n' '\n'];
fprintf(pr);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Search for nonzeros inside mask
pr=['Searching for nonzeros inside mask... \n'];
fprintf(pr)
    %Open downsampled mask (Nifti file)
mask = load_nifti(maskname);
savehdr = mask;
maskvol = mask.vol;
masksize = size(maskvol);
pr=['	Mask size: ' int2str(masksize(1)) '; ' int2str(masksize(2)) '; ' int2str(masksize(3)) '\n'] ;
fprintf(pr)
    %Search nonzeros coordinates
maskrs = reshape(maskvol,[masksize(1)*masksize(2)*masksize(3)],1);
nonzeros = find(maskrs);
pr=['	Nonzero voxels found: ' int2str(length(nonzeros)) '\n' '\n'];
fprintf(pr)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load ROI and use nonzeros-mask voxels..
pr=['Loading ~ROI: ' roiname '\n'];
fprintf(pr)
roi=load_nifti(ROIfile);
roivol = roi.vol;
roisize = size(roivol);

roirs = reshape(roivol,roisize(1)*roisize(2)*roisize(3),1);
roinonzero = roirs(nonzeros);
[m,~] = size(roinonzero);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load ind corr matrix files
    %Unzip file
pr=['Working on the adjency matrix ...' '\n'];
fprintf(pr) 
pr=['	Unzipping file ...' '\n'];
fprintf(pr)
cmd=['unzip ' corr_matrix ' -d ' pathtosavecorr];
system(cmd);
    %Load adjency matrix
pr=['	Loading adjency matrix...' '\n'];
fprintf(pr)
corrmtxname = [pathtosavecorr cname(12:end)];   
z_prev = load(corrmtxname);
z = z_prev + z_prev' - 2*z_prev.*eye(length(z_prev));
clearvars z_prev
z(find(z<0)) = 0;

    
    %Delete unziped adjency (keep the zip file.
pr=['	Deleting unziped .txt corr matrix ...' '\n' '\n'];
fprintf(pr)
delete(corrmtxname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FDR correction
bin = 0;
if bin == 1
    pr=['FDR correction' '\n'];
    fprintf(pr)
    %Apply FDR-correction to each association matrix to exclude false positives:
    ntp=120; %degrees of freedom from your original correlations time points
    p_matrix = r2pv(z,ntp); %to obtain all p-values associated to the correlation association matrix
    p_matrix=reshape(p_matrix, 1, m*m);

     index = find(p_matrix ~= 1);
     [n_signif,index_signif] = fdr(p_matrix(index), 0.05,'original','mean');%parameters for the FDR correction with a q level of 0.001
     p_matrix(:) = 0;
     p_matrix(index(index_signif)) = 1;
     p_matrix=reshape(p_matrix, m,m);
     adjency=z.*p_matrix;%FDR-corrected matrix
else
    adjency=z;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Laplacian
degreem = sum(adjency,1)';
degreem = diag(degreem); 
invdegreem = (1 ./ degreem) .^(1/2);
locinf = find(isinf(invdegreem));
invdegreem(locinf) = 0;
invdegreem(invdegreem < 0) = 0;
[m, ~]  = size(degreem);
lapm = eye(m) - invdegreem * adjency * invdegreem;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kernel diff simulation
seed = find(roinonzero > 0);

for t=0:100:1000
    kernel = exp(-t .*lapm);
    diffusion = kernel * roinonzero - sum(roinonzero);
    diffusion = sum(diffusion,2);
    vol2save= maskrs;
    vol2save(nonzeros) = diffusion(:);
    vol2savers = reshape(vol2save,masksize(1),masksize(2),masksize(3));
    savehdr.vol = vol2savers;
    save_nifti(savehdr,[pathtosaveind 'diffusion_t-' int2str(t) '_test'  '.nii']);    
    
end


end

