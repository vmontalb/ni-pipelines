#!/usr/bin/env python
# heat_kernel_diffusion.py

#
# script generates de heat diffusion of multiple subjects given a ROI.
#
# Original Author: Victor Montal
# Date_first: June-5-2017
# Date:


# ---------------------------
# Import Libraries
import numpy as np
import argparse
import os
import sys
import pdb


# ---------------------------
# Parser Options
HELPTEXT = """

heat_kernel_diffusion.py
Author: Victor Montal

toDo
"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_ddir      = 'Path where all the adjency matrices are placed.'
    h_roi       = 'Path to ROI file. SAME space as adjency matrices (e.g 8x8x8)'
    h_mask      = 'Path to mask used to compute the adjency matrix.'
    h_outdir    = 'Name of the subcortical structure. Check HELP for a name list of structures.'
    h_time      = 'Duration of simulation of kernel diffusion.'


    parser.add_argument('--ddir',dest = 'ddir',action = 'store',help = h_ddir,      required = True)
    parser.add_argument('--list',dest = 'slist',action = 'store',help = h_list,       required = True)
    parser.add_argument('--struct',dest = 'struct',action = 'store',help = h_struct,   required = True)
    parser.add_argument('--num',dest = 'num',action = 'store',help = h_num,     required = True, type = int)

    args = parser.parse_args()

    # Check options (toDo)
    return args


# ---------------------------
# Subfunctions
def my_print(message):
    """
    print message, then flush stdout
    """
    print message
    sys.stdout.flush()


def single_subj_heat_diffusion():

# ---------------------------
# Main Code
def all_heat_diffusion_simulation(options):


if __name__ == "__main__":
    options = options_parser()
    all_heat_diffusion_simulation(options)
