#!/usr/bin/env python
#!/bin/sh
#nonlocal-PET-contribution.py

##############################
#
# This script computes the whole-brain non-local contribution of PET signal in a vertex-wise manner
# (i.e for each vertex, following a diffusion network, how much does all the other veretx affect that veretx?)
#
#############################

# ==========================
# Import Libs
import os , pickle, time
import argparse, logging, subprocess, coloredlogs

import numpy as np
import scipy.io as sp
import nibabel.freesurfer.mghformat as nibmgh
import nibabel.freesurfer.io as nib
import scipy.sparse.csgraph as csgraph

import pdb


# ==========================
# Inputs
version = '0.0.1'
#logging.basicConfig(format='%(asctime)s || %(name)s >> %(levelname)s : %(message)s ', datefmt='%I:%M:%S %p')
os.environ['COLOREDLOGS_LOG_FORMAT'] = '%(asctime)s || %(name)s >> %(levelname)s : %(message)s '
os.environ['COLOREDLOGS_DATE_FORMAT'] = '%I:%M:%S %p'
coloredlogs.install(level='DEBUG')
logger = logging.getLogger('non-local eff')

description=""
usage = ''""
parser = argparse.ArgumentParser(description = description,usage = usage )

parser.add_argument('-i','--input',action = 'store', dest = 'input', help = "Surface-mesh data in fsaverage used to compute non-local contributions", required = True)
parser.add_argument('-d','--dict',action = 'store',  dest = 'dict_conv', help = 'Dictionary to convert NeMo diff matrix labels to FreeSurfer aparc labels', required = True)
args = parser.parse_args()

# Sanitise inputs


# ==========================
# Main code
def nonLocal():
    logger.info("Starting to compute non-local effects ...")

    # - Load dict_conv and revers (i.e from )
    logger.info("   Loading conversion dictionary: from NeMo to Freesurfer LUT ")
    with open('/home/neuroimagen2/Documentos/scripts/Diffusion-Network/conversion_dict.pkl', 'rb') as f:
        dickt =  pickle.load(f)
    idickt = [dict((v,k) for k,v in dickt.iteritems()) ][0]

    # - Load Connectivity matrix and normalize
    logger.info("   Loading Diffusion Network Matrix and normalizing it ... ")
    mat = sp.loadmat('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Tools/NeMo-master/Tractograms/FiberTracts86_MNI_BIN/TRS_matrix.mat')
    connmat = mat['fTRS']
    sumconn = np.sum(connmat, axis = 1)
    conn = np.divide(connmat, sumconn[:,np.newaxis] )

    # - Load aparc+aseg fsaverage .annot file
    logger.info("   Loading ?h.aparc.annot files ")
    [labelslh, ctablh, nameslh] = nib.read_annot('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/lh.aparc.annot')
    [labelsrh, ctabrh, namesrsh] = nib.read_annot('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/rh.aparc.annot')
    annotlabels = np.append(np.add(labelslh,1000), np.add(labelsrh,2000))

    # - Convertin aparc.annot labels to NeMo labels
    nemolabels = np.multiply(annotlabels, 0)
    for idxs,value in enumerate(annotlabels):
        try:
            nemolabels[idxs] = idickt[value]
        except Exception, e:
            nemolabels[idxs] = -1

    # - Load surface
    logger.info("   Loading Loading surfaces ... ")
    origsurflh  = nibmgh.load('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/002_S_4262/002_S_4262_lh_fsaverage-smth0-tau.mgh')
    surflh = origsurflh.get_data()
    origsurfrh  = nibmgh.load('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/002_S_4262/002_S_4262_rh_fsaverage-smth0-tau.mgh')
    surfrh = origsurfrh.get_data()
    surf = np.append(surflh, surfrh)

    # - Compute the vertex non-local effect using a ROI basis_type
    #       This means that, for each ROI, we will  (1) search all the vertex in that ROI; (2) Compute the
    #       connectivity b/w that ROI and all other ROIs and (3) Compute the non-local effect for all the
    #       vertex in that specific ROI that will be shared for all vertex inside current ROI.
    #       Using this strategy, we repeat the proces Z ROIs times instead of N vertex times.
    outsurf = np.zeros_like(surf)
    for idx, ROI in enumerate(np.unique(nemolabels)[1:]): # Skip  ROI value -1 (not in NeMo conn matrix) <-- the corpus callosum
        logger.info("       Working w/ ROI: "+str(ROI))

        ## -- Search vertex position for certain ROI
        ROIpos = np.where(nemolabels == ROI)[0]

        ## -- Get connectivity between current ROI and all other ROIs
        connroi = conn[ROI - 1 , :]  # Get rid of Matlab indexation (substract 1)

        ## -- Assign vertex-wise connectivity (based on ROI)
        connvertex = np.zeros_like(nemolabels, dtype=np.float32)
        for idx2,label in enumerate(nemolabels):
            if label != -1:
                connvertex[idx2] = connroi[label-1]
            else:
                connvertex[idx2] = 0
        ## -- Compute the non-local effect for all the vertex in current ROI
        outsurf[ROIpos] = np.dot(connvertex, surf.T)

    # - Normalize values in order to make them comparable to other
    logger.info("   Normalizing values to [0,1] (i,e dividing by the max) ")
    maxval = np.max(outsurf)
    nonlocsurfnorm = np.divide(nonlocsurf, maxval)

    # - Save results
    logger.info("   Saving results  ... ")


    logger.info('------')
    logger.info(' DONE ')
    logger.info('------')

# ==========================
# Extra functions
def sigmoid(x):
    s = 1.0 / (1.0 + np.exp(np.multiply(-1.0 , x)))
    return s

# ==========================
# Run code
if __name__ == "__main__":
    nonLocal()


# ==========================
# BONUS ANLAYISIS --> EIGENVALUE DECOMPOSITION FOR ATROPHY PATTERN
"""
import scipy.io as sp
mat = sp.loadmat('blabla.mat')
connmat = mat['fTRS']
lap = csgraph.laplacian(connmat, normed = True)

[W, V] = np.linalg.eig(lap) # w --> eigenvalue, V --> eigenvector

"""
