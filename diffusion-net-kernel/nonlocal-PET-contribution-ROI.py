#!/usr/bin/env python
#!/bin/sh
#nonlocal-PET-contribution-ROI.py

##############################
#
# This script computes the whole-brain non-local contribution of PET signal in a ROI-wise manner
# (i.e for each ROI, following a diffusion network, how much does all the other veretx affect that veretx?)
#
#############################

# ==========================
# Import Libs
import os , pickle, time
import argparse, logging, subprocess, coloredlogs

import numpy as np
import scipy.io as sp
import nibabel.freesurfer.mghformat as nibmgh
import nibabel.freesurfer.io as nib
import scipy.sparse.csgraph as csgraph

import pdb


# ==========================
# Inputs
version = '0.0.1'
#logging.basicConfig(format='%(asctime)s || %(name)s >> %(levelname)s : %(message)s ', datefmt='%I:%M:%S %p')
os.environ['COLOREDLOGS_LOG_FORMAT'] = '%(asctime)s || %(name)s >> %(levelname)s : %(message)s '
os.environ['COLOREDLOGS_DATE_FORMAT'] = '%I:%M:%S %p'
coloredlogs.install(level='DEBUG')
logger = logging.getLogger('non-local eff')

description=""
usage = ''""
parser = argparse.ArgumentParser(description = description,usage = usage )

parser.add_argument('-i','--input',action = 'store', dest = 'input', help = "Surface-mesh data in fsaverage used to compute non-local contributions", required = True)
parser.add_argument('-d','--dict',action = 'store',  dest = 'dict_conv', help = 'Dictionary to convert NeMo diff matrix labels to FreeSurfer aparc labels', required = True)
args = parser.parse_args()

# Sanitise inputs

# ==========================
# Main code
def nonLocal():
    logger.info("Starting to compute non-local effects ...")

    # - Load dict_conv and revers (i.e from )
    logger.info("   Loading conversion dictionary: from NeMo to Freesurfer LUT ")
    with open('/home/neuroimagen2/Documents/scripts/Diffusion-Network/conversion_dict.pkl', 'rb') as f:
        dickt =  pickle.load(f)
    idickt = [dict((v,k) for k,v in dickt.iteritems()) ][0]

    # - Load Connectivity matrix and normalize
    logger.info("   Loading Diffusion Network Matrix and normalizing it ... ")
    mat = sp.loadmat('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Tools/NeMo-master/Tractograms/FiberTracts86_MNI_BIN/TRS_matrix_old.mat')
    connmat = mat['fTRS']
    sumconn = np.sum(connmat, axis = 1)
    conn = np.divide(connmat, sumconn[:,np.newaxis] )

    # - Load aparc+aseg fsaverage .annot file
    logger.info("   Loading ?h.aparc.annot files ")
    [labelslh, ctablh, nameslh] = nib.read_annot('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/lh.aparc.annot')
    [labelsrh, ctabrh, namesrsh] = nib.read_annot('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/fsaverage/label/rh.aparc.annot')
    annotlabels = np.append(np.add(labelslh,1000), np.add(labelsrh,2000))

    # - Converting aparc.annot labels to NeMo labels
    logger.info("   Converting aparc to NeMo labels  ")
    nemolabels = np.multiply(annotlabels, 0)
    for idxs,value in enumerate(annotlabels):
        try:
            nemolabels[idxs] = idickt[value]
        except Exception, e:
            nemolabels[idxs] = -1

    # - Load surface
    logger.info("   Loading surfaces ... ")
    origsurflh  = nibmgh.load('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Ab-tau_nonlocal/Raj-based/FBP_surface/002_S_4213/002_S_4213_lh_fsaverage-smth0-flor.mgh')
    surflh = origsurflh.get_data()
    origsurfrh  = nibmgh.load('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Ab-tau_nonlocal/Raj-based/FBP_surface/002_S_4213/002_S_4213_rh_fsaverage-smth0-flor.mgh')
    surfrh = origsurfrh.get_data()
    surf = np.append(surflh, surfrh)

"""
surflh  = nib.read_morph_data('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/toy_input/lh.tau_enthorinal.toyexample.sulc')
surfrh  =  nib.read_morph_data('/media/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Diffusion-network/Analysis/toy_input/rh.tau_enthorinal.toyexample.sulc')
surf = np.append(surflh, surfrh)
"""

    # - Compute surface-ROI mean
    logger.info("   Computing surface ROI mean  ... ")
    allrois = np.unique(nemolabels)[1:]
    numrois = allrois.size
    meanroi = np.zeros([numrois])
    for idx, roi in enumerate(allrois):
        pos = np.where(nemolabels == roi)[0]
        meanroi[idx] = np.mean(surf[pos])

    # - Compute the non-local effect
    logger.info("   Computing the non-local effect for each ROI  ... ")
    roiconn = conn[0:68, 0:68]
    nolocal = np.dot(meanroi, roiconn)

    # - Normalize values in order to make them comparable to other
    logger.info("   Normalizing values to [0,1] (i,e dividing by the max) ")
    maxval = np.max(nolocal)
    nolocalnorm = np.divide(nolocal, maxval)

    # - Project results to surface
    logger.info("   Moving to surface ROI  ... ")
    outsurf = np.zeros_like(surf)
    for idx, roi in enumerate(allrois):
        pos = np.where(nemolabels == roi)[0]
        outsurf[pos] = nolocalnorm[roi-1]

    # - Save Results
    logger.info("   Saving Results  ... ")
    nib.write_morph_data('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Ab-tau_nonlocal/Raj-based/FBP_surface/002_S_4213/002_S_4213_lh_fsaverage-smth0-non-local-flor.sulc', outsurf[:163842, np.newaxis, np.newaxis])
    nib.write_morph_data('/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/Ab-tau_nonlocal/Raj-based/FBP_surface/002_S_4213/002_S_4213_rh_fsaverage-smth0-non-local-flor.sulc', outsurf[163842:, np.newaxis, np.newaxis])


    logger.info('------')
    logger.info(' DONE ')
    logger.info('------')

# ==========================
# Extra functions
def sigmoid(x):
    s = 1.0 / (1.0 + np.exp(np.multiply(-1.0 , x)))
    return s

# ==========================
# Run code
if __name__ == "__main__":
    nonLocal()
