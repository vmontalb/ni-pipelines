#!/usr/bin/env python
# synthetic_PET_radon.py

#
# Pipeline to preprocess FBP for volume-based/surfacebased explorations: from native NIFTI to MNI and surface.
#
# Original Author: Victor Montal
# Date_first: Feb-2-2018
# Date:


# ---------------------------
# Import Libraries
from __future__ import print_function
import argparse
import os
from os.path import join
import time
import shutil
import sys
import subprocess
import pdb

import numpy as np
import nibabel as nib
from nibabel.nifti1 import load
from skimage.transform import radon, iradon, iradon_sart
import matplotlib.pyplot as plt
from scipy.ndimage.filters import uniform_filter1d
from scipy.signal import medfilt



# ---------------------------
# Parser Options
HELPTEXT = """

PET_processing.py

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat


>> This script performs all the required steps to obtain PET volumes in both the MNI
space and the native + fsaverage surfaces to perform fast analysis. It also computes
partial volume corrections using different approaches. <<

http://scikit-image.org/docs/dev/auto_examples/transform/plot_radon_transform.html
STEPS:
------


REFS:
-----

[1]


[2]


[3]



"""

USAGE = """

"""


def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_invol = 'Input FDG volume (GM, WM, CSF and Skull constants)'
    h_fspath = 'Path to Freesurfer subjects dir.'
    h_outpath = 'Path where syntehtic volume will be saved'
    h_debug = 'Save sinogram volumes'

    parser.add_argument('--invol',      dest = 'invol',     action = 'store',       help = h_invol,   required = True)
    parser.add_argument('--fspath',     dest = 'fspath',    action = 'store',       help = h_fspath,   required = True)
    parser.add_argument('--outpath',    dest = 'outpath',   action = 'store',       help = h_outpath,   required = True)
    parser.add_argument('--debug',      dest = 'debug',     action = 'store_true',  help = h_debug,     required = False, default = False)


    args = parser.parse_args()
    fshome = os.environ['FREESURFER_HOME']
    with open(join(fshome, 'build-stamp.txt'), 'r') as f:
        version = f.read()
        if 'stable-pub-v6' not in version:
            print('----------------------------------------------------------------------------------------------------------------')
            print('WARNING: YOU ARE NOT USING FREESURFER VERSION 6. THIS WILL CAUSE ERRORS DURING THE PROCESSING.')
            print('----------------------------------------------------------------------------------------------------------------')
            sys.exit(1)

    return args


# ---------------------------
# Subfunctions
def my_print(message, ff):
    """
    print message, then flush stdout
    """
    print(message)
    ff.write(message)
    sys.stdout.flush()

def run_cmd(cmd,err_msg,ff):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)

def opennifti(vol):
    img = load(vol)
    data = img.get_data()
    affine = img.affine
    shape = data.shape
    return data,affine,shape


# ---------------------------
# Main Code
def synthetic_vol(options):
    startime = time.time()
    subjpath = os.path.dirname(options.invol)
    tosave = subjpath
    logfile = join(tosave, 'log_file.log')
    log = open(logfile, 'w')

    # Load volume
    my_print('> Read input volume (constant GM, WM, CSF and skull values) \n', log)
    [vol,volaff,volshape] = opennifti(options.invol)

#        #___________________Radon transform + noise + smooth sinogram___________________
    # Apply radon Transform
    my_print('> Preparing file to perform Radon transformation (sinogram) \n' , log)
    theta = np.linspace(0., 180., volshape[1], endpoint=False)
    fsinogram = np.zeros_like(vol)
    recvol = np.zeros_like(vol)
    for idxslice in range(0,volshape[2]):
        if np.mod(idxslice,10) == 0:
            my_print('>     Working with PET slice '+str(idxslice)+'', log)
        currslice = vol[:,:,idxslice]
        currsinogram = radon(currslice,theta = theta, circle=True)

        # Viz sinogram (just if debug option on)
        """
        if options.debug:
            fig, ax = plt.subplots()
            ax.imshow(currsinogram, cmap=plt.cm.Greys_r, extent=(0, 180, 0,
                        sinogram.shape[0]), aspect='auto')
            fig.tight_layout()
            plt.show()
        """

        # Smooth sinogram
        smoothsinogram = np.zeros_like(currsinogram)
        ranges = np.round(np.divide(currsinogram.shape[1], 12))
        for col in range(0,currsinogram.shape[1]):
            # Check how many smoothing iterations depending on how far from center signal is
            if col > ranges*5 and col < ranges * 7:
                niter = 1
            else:
                niter = 2

            # Apply 1D median filter (kernel 3) niter times
            for iterat in range(0,niter):
                tmp = uniform_filter1d(tmp, 3)
            smoothsinogram[:,col] = tmp

        # Add Poison noise
        negmask = smoothsinogram < 0 # Need to mask neg values close to zero to avoid prob w/ poisson
        smoothsinogram[negmask] = 0
        noise_mask = np.random.poisson(smoothsinogram)
        noisy_sinogram = np.divide(noise_mask,100) + smoothsinogram

        # Reconstruct image
        fsinogram[:,:,idxslice] = noisy_sinogram
        """
        Reconstruction using basic algorithm of Filtered Back Projection
        currrec = iradon(fsinogram[:,:,idxslice], theta=theta, circle=True)
        recvol[:,:,idxslice] = currrec
        """
        tmp = iradon_sart(fsinogram[:,:,idxslice].astype(np.double), theta=theta.astype(np.double))
        for iter in range(0,5):
            tmp = iradon_sart(fsinogram[:,:,idxslice].astype(np.double), theta=theta, image=tmp)
        recvol[:,:,idxslice] = tmp

    # Save image (both smoothed and raw)
    nib.save(nib.Nifti1Image(fsinogram,volaff),join(tosave,'senogram.nii.gz'))
    subjname = os.path.basename(tosave)
    nib.save(nib.Nifti1Image(recvol,volaff),join(tosave,'synthetic_sinogram_'+subjname+'.nii.gz'))

        #___________________Add noise to the transformation (-1 to 1 degrees)___________________
    # Add noise to the registration
    my_print('> Add error to the registration between PET and T1 \n', log)
    # For future papers, check paper: https://www.sciencedirect.com/science/article/pii/S0730725X10000937
    # Transform .lta to fsl
    cmd = "lta_convert  --inlta "+join(tosave,'fdg_to_t1.reg.lta')+" \
                        --outfsl "+join(tosave,'fdg_to_t1.reg.fsl')
    run_cmd(cmd, "Error converting from lta to fsl", log)

    # Generate one random with slight rotation using makerot
    randtheta = np.multiply(np.round(np.random.rand(), decimals=3), 2)
    randaxis = np.random.randint(0,3, dtype=np.int)
    randdirection = np.random.randint(-1,1)
    axis=np.zeros(3, dtype=np.int)
    axis[randaxis] = 1
    tmp=''
    axis = [tmp + str(z) for z in axis]
    axis = ','.join(axis)

    cmd = "makerot  -t "+str(randtheta)+" \
                    -a "+axis+" \
                    -c "+options.invol+" \
                    -o "+join(tosave, 'rotmat.mat')
    run_cmd(cmd, "Error generating random rotation matrix using makerot",log)
    # Concatenate both transformations (original+random)
    cmd = "mri_matrix_multiply  -fsl -im "+join(tosave, 'rotmat.mat')+" \
                                -im "+join(tosave,'fdg_to_t1.reg.fsl')+" \
                                -om "+join(tosave,'concat_rot.fsl')
    run_cmd(cmd, "Error concatenating transformation matrix",log)
    cmd = 'lta_convert --infsl '+join(tosave,'concat_rot.fsl')+'\
                        --outlta '+join(subjpath,'noise_rot_fdg_to_t1.reg.lta')+" \
                        --src "+options.invol+ " \
                        --trg "+join(options.fspath,subjname,'mri','brainmask.mgz')
    run_cmd(cmd, 'Error transforming the concatenated rot matrix from FSL to .lta', log)

    my_print("> Generating a pausible synthetic FDG volume done.", log)
    my_print(".. If you want to check the difference between the slight rotated and the original affine registration: ", log)
    my_print(".. freeview -v "+join(options.fspath,subjname,'mri','brainmask.mgz')+" \
    -v "+join(tosave,'synthetic_sinogram_'+subjname+'.nii.gz')+":reg="+join(subjpath,'noise_rot_fdg_to_t1.reg.lta')+" \
    -v "+join(tosave,'synthetic_sinogram_'+subjname+'.nii.gz')+":reg="+join(tosave,'fdg_to_t1.reg.lta')+" \
    -f "+join(options.fspath,subjname,'surf','lh.white')+" \
    -f "+join(options.fspath,subjname,'surf','rh.white'), log)


if __name__ == "__main__":
    options = options_parser()
    synthetic_vol(options)
