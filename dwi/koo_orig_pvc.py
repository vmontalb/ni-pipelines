#!/usr/bin/env python
# koo_pvc_orig.py

#
# script to prepare folder to run GLM with custom .mgh files (i.e DONSURF)
#
# Original Author: Victor Montal
# Date_first: June-5-2017
# Date:


# ---------------------------
# Import Libraries
import numpy as np
import scipy.io as sio
import argparse
import os
import shutil
import sys
import subprocess
from nibabel.nifti1 import load
import nibabel as nib
import pdb

# ---------------------------
# Parser Options
HELPTEXT = """

prepare_GLM.py
Author: Victor Montal

toDo HELP txt

"""

USAGE = """

"""
def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_md    = 'Volume 3D Mean Diffusivity file. (native space)'
    h_pvf   = 'Partial Volume fraction file 4D.(native space)'
    h_TR    = 'TR value of the acq DWI (in ms)'
    h_TE    = 'TE value of the acq DWI (in ms)'
    h_bval  = 'b-value value of the acq DWI'


    parser.add_argument('--md',     dest = 'md',    action = 'store',   help = h_md,    required = True)
    parser.add_argument('--pvf',    dest = 'pvf',   action = 'store',   help = h_pvf,   required = True)
    parser.add_argument('--tr',     dest = 'tr',    action = 'store',   help = h_TR,    required = True)
    parser.add_argument('--te',     dest = 'te',    action = 'store',   help = h_TE,    required = True)
    parser.add_argument('--bval',   dest = 'bval',  action = 'store',   help = h_bval,  required = True)



    args = parser.parse_args()

    # Check options (toDo)
    return args


# ---------------------------
# Subfunctions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

# ---------------------------
# Main Code
def koo_pvc(options):

    # - Load MD and pvf volumes
    my_print(" \n Loading MD and pvf files.")
    mdraw = load(options.md).get_data()
    mdaff = load(options.md).affine
    rsmd = mdraw.reshape(mdraw.shape[0] * mdraw.shape[1] * mdraw.shape[2])

    pvf = load(options.pvf).get_data()
    rspvf = pvf.reshape(pvf.shape[0] * pvf.shape[1] * pvf.shape[2], pvf.shape[3])

    # - Sum GM and WM
    my_print(" \n Sum gm + wm percent and filter MD by 30 percent.")
    gmwm = rspvf[:,0] + rspvf[:,2]
    idxgm = np.where(rspvf[:,0] > 0.3)[0]

    csf = rspvf[:,3] + rspvf[:,4]

    # - Define Koo parameters
    TR = float(options.tr)
    TE = float(options.te)
    b = float(options.bval)

    spindengm = 0.88
    spindencsf = 1.
    T1gm = 1160.
    T2gm = 83.
    T1csf = 4163.
    T2csf = 2000.

    # - compute aparent percentage
    my_print(" \n Computing apparent signal fraction. ")
    S0gm = spindengm * (np.exp(np.divide(-TE, T2gm))) * (1 - np.exp(np.divide(-TR, T1gm)))
    S0csf = spindencsf * (np.exp(np.divide(-TE, T2csf))) * (1 - np.exp(np.divide(-TR, T1csf)))

    appgm = (gmwm[idxgm] * S0gm) / ((csf[idxgm] * S0csf) + (gmwm[idxgm] * S0gm))
    appcsf = np.subtract(1 , appgm)

    # - Compute MDgm
    my_print(" \n Computing cortical corrected MD. ")
    mdgm = np.zeros_like(rsmd)
    csfcompart = appcsf * np.exp(-b * 0.003)
    tmp = np.divide((np.exp(-b * rsmd[idxgm]) - csfcompart) , appgm)
    tmp[tmp < 0] = 0
    mdgm[idxgm] = np.divide(np.log(tmp) , -b)

    # - Save MDgm volume
    my_print(" \n Saving final volume. ")
    savef = options.md[:-7]+'_koo.nii.gz'
    rsmdgm = mdgm.reshape(mdraw.shape[0] , mdraw.shape[1] , mdraw.shape[2])
    nib.save(nib.Nifti1Image(rsmdgm,mdaff),savef)


if __name__ == "__main__":
    options = options_parser()
    koo_pvc(options)
