#!/usr/bin/env python
# donsurf1.0.0_umcluster.py

#
# Pipeline to compute surface-based DWI metrics
#
# Original Author: Victor Montal
# Date_first: March-14-2022
# Date:

# ---------------------------
# Import Libraries
# ---------------------------
from __future__ import print_function
import numpy as np
import scipy.io as sio
import argparse
import os
from datetime import datetime
import time
from os.path import join
from os.path import dirname
import json
import time
import shutil
import sys
import subprocess
import logging

import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from nibabel.nifti1 import load
import nibabel as nib
from nibabel.freesurfer import read_geometry

import pdb

# ---------------------------
# Parser Options
# ---------------------------
HELPTEXT = """

donsurf1.0.0_umcluste.py [dev version]

Author:
------
Victor Montal
vmontalb [at] protonmail [dot] cat


>> Script to run basic PREPROCESSING of DWI data <<

STEPS:
------
 0.- Software version logging.
 1.- Boundary-based registration B0-to-T1 [1]
 2.- (if) Partial volume correction (Koo et al) of MD maps [2]
 3.- Surface sampling at mid-point cortical ribbon [3,4]
    3.1.- GM-MD, SWM-MD,(if) GM-MD-koo (if) GM-ICVF, GM-ISOVF, GM-OD
 4.- Surface-based registration to fsaverage
 5.- Create smoothing mask (exclude non-signal regions)
 6.- Smooth with fwhm=10,15
 7.- Copy files to Freesurfer subject's folder


REFS:
-----
[1] Greve DN, Fischl B.
    "Accurate and robust brain image alignment using boundary-based registration"
    (2009) Neuroimage.15;48(1):63-72. doi: 10.1016/j.neuroimage.2009.06.060

[2] Koo BB, Hua N, Choi CH, Ronen I, Lee JM, Kim DS
    "A framework to analyze partial volume effect on gray matter
           mean diffusivity measurements"
    (2009) Neuroimage. 2009 Jan 1;44(1):136-44. doi: 10.1016/j.neuroimage.2008.07.064.

[3] Coalson TS, Van Essen DC, Glasser MF.
    "The impact of traditional neuroimaging methods on the spatial localization
          of cortical areas"
    (2018) Proc Natl Acad Sci, doi: 10.1073/pnas.1801582115

[4] Montal V, Vilaplana E, Alcolea D, Pegueroles J et al
    "Cortical microstructural changes along the Alzheimer's disease continuum"
    (2018) Alzheimers Dement   doi: 10.1016/j.jalz.2017.09.013

"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_bidspath  = 'Full path to subject (BIDS) sourcedata dwi data.'
    parser.add_argument('-p',  dest = 'subjectpath', action = 'store', help = h_bidspath, required = True)

    args = parser.parse_args()

    # Check .json file does not exist

    return args

# ---------------------------
# Subfunctions
# ---------------------------
def my_print(message, ff):
    """
    print message, then flush stdout
    """
    print(message)
    ff.write(message+'\n')
    sys.stdout.flush()

def run_cmd(cmd,err_msg,ff):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)

def seconds2min(ctime):
    seconds = ctime % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    strtime = "{hour}h: {min}min: {sec}s".format(hour=hour,min=minutes,sec=round(seconds))

    return strtime

def multiviewMRIplot(mrivol, lsurf, rsurf, outname):
    """
    Plot MRI data with surface overlayed (?h.white)

    Inputs:
    -------
      - mrivol   :
      - lwhite   :
      - rwhite   :
      - outname  :

     Inspired by:
     https://mne.tools/0.21/auto_tutorials/source-modeling/plot_background_freesurfer_mne.html
    """
    # Load MRI vol
    mri = nib.load(mrivol)
    mridata = mri.get_fdata()

    # Load surface data
    lhcoord, lhface = read_geometry(lsurf)
    rhcoord, rhface = read_geometry(rsurf)

    # Transform surface pos to plot correctly
    Torig = mri.header.get_vox2ras_tkr()
    invTorig = np.linalg.inv(Torig)
    lhvox = np.dot(lhcoord, invTorig[:3, :3].T)  # Apply transform
    lhvox += invTorig[:3, 3] # Apply translation
    rhvox = np.dot(rhcoord, invTorig[:3, :3].T)  # Apply transform
    rhvox += invTorig[:3, 3] # Apply translation
    vox = np.vstack((lhvox,rhvox))
    tris = np.vstack((lhface,np.max(lhface) + 1 +rhface))   # We shift the triangles vertices to not overlap triangles

    # Pre-define plots
    voxpos = [80,95,110,125]
    voxposvis = [135,150,165,175]
    fig, ax = plt.subplots(4, 4, figsize=(30,30))
    fig.set_tight_layout({'pad': 0})
    fig.subplots_adjust(wspace=0)

    # View-1
    for idx,cpos in enumerate(voxpos):
        toplot = mridata[cpos,:,:]
        ax[0,idx].imshow(toplot, cmap='gray') # MRI image
        ax[0,idx].axis('off')
        ax[0,idx].tricontour(vox[:, 2], vox[:, 1], tris, vox[:, 0],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[0,idx].set_aspect('equal')

    # View-1
    for idx,cpos in enumerate(voxposvis):
        toplot = mridata[cpos,:,:]
        ax[1,idx].imshow(toplot, cmap='gray') # MRI image
        ax[1,idx].axis('off')
        ax[1,idx].tricontour(vox[:, 2], vox[:, 1], tris, vox[:, 0],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[1,idx].set_aspect('equal')

    # View-3
    for idx,cpos in enumerate(voxpos):
        toplot = mridata[:,:,cpos]
        toplot = toplot.swapaxes(0,1)
        ax[2,idx].imshow(toplot, cmap='gray')
        ax[2,idx].axis('off')
        ax[2,idx].tricontour(vox[:, 0], vox[:, 1] , tris, vox[:, 2],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[2,idx].set_aspect('equal')

    # View-3
    for idx,cpos in enumerate(voxposvis):
        toplot = mridata[:,:,cpos]
        toplot = toplot.swapaxes(0,1)
        ax[3,idx].imshow(toplot, cmap='gray')
        ax[3,idx].axis('off')
        ax[3,idx].tricontour(vox[:, 0], vox[:, 1] , tris, vox[:, 2],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[3,idx].set_aspect('equal')
    # Save fig
    #fig.subplots_adjust(0.1, 0.1, 0.95, 0.85)
    fig.savefig(outname)

# ---------------------------
# Main Code
# ---------------------------
def donsurf(options):
    """
    """
    globtime = time.time()
    # =========================
    # Parse (used) Software versions
    # =========================
    fslv = os.path.basename(dirname(os.environ['FSLDIR']))

    # =========================
    # Define input variables based on BIDS and .json
    # =========================
    dwipreprocversion = "dwipreproc1.0.0"
    donsurfversion = "donsurf1.0.0"
    freesurferversion = "freesurfer7.1.1"

    bidspath = dirname( dirname( dirname( dirname(options.subjectpath))))
    derivatives = join(bidspath,'derivatives')

    subj = os.path.basename( dirname( dirname( options.subjectpath)))
    visit = os.path.basename( dirname( options.subjectpath))
    csubject = subj+"_"+visit

    dwipreprocpath = join(derivatives,subj,visit,'dwi',dwipreprocversion)
    dwipreprocjson = join(dwipreprocpath,dwipreprocversion+'.json')

    outputd = join(derivatives,subj,visit,'dwi',donsurfversion)

    fspath = join(derivatives,subj,visit,'anat',freesurferversion)
    os.environ['SUBJECTS_DIR'] = fspath
    fshome = os.environ['FREESURFER_HOME']

    # If multiple freesurfer, use the manual edit version
    fssubjects = [d for d in os.listdir(fspath) if (os.path.isdir(join(fspath,d)) and d.startswith("freesurfer")) ]
    if len(fssubjects) > 1:
        fssubj = [d for d in fssubjects if d.endswith("edits")]
        fssubj = fssubj[0]
    else:
        fssubj = fssubjects[0]

    # =========================
    # Check subject not running in queue
    # =========================
    outjson = join(outputd,donsurfversion+'.json')
    if os.path.exists(outjson):
        print("Subject already in Slurm queue")
        #exit()

    # =========================
    # Parse DWI input info
    # =========================
    jsonf = open(dwipreprocjson,'r')
    datajson = json.load(jsonf)
    jsonf.close()

    # Parse parameters json
    te = float(datajson['EchoTime(ms)'])
    tr = float(datajson['RepetitionTime(ms)'])
    bvals = datajson['Bvalue']
    multishell = datajson['MultiShell']

    if multishell != "True":
        bvalfile = datajson['Bval']
        bvalkoo = np.loadtxt(bvalfile, dtype = int)
        bvalkoo = np.unique(bvalkoo)[1]   # get bval

        metrics = ['GM-MD','SWM-MD','GM-MD-koo']
    else:
        metrics = ['GM-MD','SWM-MD','GM-ICVF','GM-ISOVF', 'GM-OD']

    # =========================
    # Generate output structure
    # =========================
    os.makedirs(outputd,exist_ok = True)
    for cmetric in metrics:
        os.makedirs(join(outputd,cmetric,'labels'),exist_ok = True)
        shutil.copy(join(fspath,'fsaverage','label','lh.cortex.label'), join(outputd,cmetric,'labels','lh.cortex.label'))
        shutil.copy(join(fspath,'fsaverage','label','rh.cortex.label'), join(outputd,cmetric,'labels','rh.cortex.label'))

    os.makedirs(join(outputd,'reg'),exist_ok = True)
    os.makedirs(join(outputd,'QC'),exist_ok = True)
    os.makedirs(join(outputd,'tmp'),exist_ok = True)

    # ========================
    # LOG software versions and input params
    # =========================
    logfile = join(outputd, 'log_file.log')
    log = open(logfile, 'w')
    logparams = '''\
++++++++++++++++++++++++++++++++++++
+ DONSURF 1.0.0 PIPELINE +
++++++++++++++++++++++++++++++++++++

SOFTWARE VERSION:
-----------------
 · Freesurfer-version    : {fsversion}
 · DWIpreproc-version    : {dwipreprocversion}


PIPELINE PARAMETERS:
--------------------
 · Input DWIpreproc      : {inputdwi}
 · Freesurfer            : {subjFS}
 · Freesurfer Path       : {pathFS}
 · Output dir            : {outdir}

 · Multishell            : {multishell}

 · Metrics               : {cmetrics}


DWI PARAMETERS:
---------------
 · B-value         : {bval}
 · TE              : {te}
 · TR              : {tr}

    '''.format(fsversion=freesurferversion, dwipreprocversion=dwipreprocversion,
                inputdwi=dwipreprocpath, subjFS=fssubj, pathFS=fspath, outdir=outputd,
                multishell=multishell, cmetrics=metrics, bval=bvals, te=te, tr=tr)

    # =========================
    # Declare empty .Json
    # =========================
    ## This is needed to avoid running the script twice in the cluster
    outjson = join(outputd,'donsurf1.0.0.json')
    open(outjson, 'a').close()

    # =========================
    # Run donsurf
    # =========================
    # - B0 to T1 registration
    # ------------------------
    # Run bbregister
    my_print('\n\nComputing EPI to T1 registration.\n\n', log)
    cstime = time.time()
    my_print('  >Running bbregister. \n', log)
    cmd = 'bbregister   --s '+str(fssubj)+' \
                        --mov '+join(dwipreprocpath,"b0_img.nii.gz")+'\
                        --reg '+join(outputd,'reg', 'register.dat')+' \
                        --dti \
                        --6'
    run_cmd(cmd, 'Registration between EPI and T1 failed!', log)

    my_print('  >Transforming .dat regitration matrix to .lta file. \n', log)
    cmd = 'lta_convert  --inreg '+join(outputd,'reg', 'register.dat')+' \
                        --outlta '+join(outputd,'reg', 'register.lta')+' \
                        --src '+join(dwipreprocpath,"b0_img.nii.gz")+' \
                        --trg '+join(fspath,fssubj,'mri','brainmask.mgz')
    run_cmd(cmd, 'Reg format conversion failed!', log)
    cetime = time.time()
    timeprint = seconds2min(-cstime+cetime)
    my_print(".   Elapsed time EPI to T1 reg: "+timeprint, log)

    # Perform screenshot QC registration
    my_print('  >Performing QC screenshots of the DWI to T1 registration. \n', log)
    cstime = time.time()
    # Transform b0 to T1-FS space
    cmd = "mri_vol2vol  --mov "+join(dwipreprocpath,"b0_img.nii.gz")+" \
                        --targ "+join(fspath,fssubj,'mri','brainmask.mgz')+" \
                        --o "+join(outputd,"QC","b0-T1fs-space.mgz")+" \
                        --lta "+join(outputd,'reg', 'register.lta')
    run_cmd(cmd, 'Normalization of B0 to T1-FS failed!', log)
    # Perform screenshots
    lhsurf = join(fspath,fssubj,'surf','lh.pial')
    rhsurf = join(fspath,fssubj,'surf','rh.pial')
    invol = join(outputd,"QC","b0-T1fs-space.mgz")
    outfile = join(outputd,"QC","b0-T1fs-reg_QC.png")
    multiviewMRIplot(invol, lhsurf, rhsurf, outfile)
    cetime = time.time()
    timeprint = seconds2min(-cstime+cetime)
    my_print(".   Elapsed time QC ss reg: "+timeprint, log)

    # - (if) compute GM-MD-koo
    # ------------------------
    if multishell != "True":
        my_print('\nComputing the partial volume correction of the MD using Koo et al 2009.\n\n', log)
        cstime = time.time()
        gtmfile = join(fspath, fssubj, 'mri', 'gtmseg.mgz')
        if not os.path.isfile(gtmfile):
            my_print('      >> Computing fine segmentation. \n', log)
            cmd = 'gtmseg --s '+fssubj
            run_cmd(cmd, 'Unable to compute fine segmentation using Freesurfers gtmseg.', log)

        my_print('  >Computing the partial volume fraction for each voxel in native DWI space.\n', log)
        cmd = 'mri_gtmpvc --i '+join(dwipreprocpath,"b0_img.nii.gz")+' \
                        --reg '+join(outputd,'reg', 'register.lta')+' \
                        --psf 0 \
                        --seg gtmseg.mgz \
                        --default-seg-merge \
                        --mgx .01 \
                        --no-rescale \
                        --o '+join(outputd,'tmp')
        run_cmd(cmd,'Failed to compute PVC using mri_gtmpvc', log)
        shutil.copy(join(outputd,'tmp','aux','tissue.fraction.psf.nii.gz'),join(outputd,'GM-MD-koo', 'pvf.nii.gz'))

        my_print('  >Running the Koo et al 2009 Partial Volume Correction\n', log)
        pvcscript="/data/theshire/nicore/DATA/SPIN/0_script/3_build_derivatives/pipelines/donsurf1.0.0/koo_orig_pvc.py"
        cmd = 'python '+pvcscript+ ' --md='+join(dwipreprocpath,'DTI','dti_MD.nii.gz')+' \
                                     --pvf='+join(outputd,'GM-MD-koo', 'pvf.nii.gz')+' \
                                     --tr='+str(tr)+' \
                                     --te='+str(te)+' \
                                     --bval='+str(bvalkoo)
        run_cmd(cmd, 'PVC code failed!', log)
        shutil.move(join(dwipreprocpath,'DTI','dti_MD_koo.nii.gz'),join(outputd,'GM-MD-koo','dti_MD_koo.nii.gz') )

        my_print('  >Thresholding the PVC corrected image.\n', log)
        cmd = 'fslmaths '+join(outputd,'GM-MD-koo','dti_MD_koo.nii.gz')+' \
                        -thr 0 \
                        '+join(outputd,'GM-MD-koo','dti_MD_koo.nii.gz')
        run_cmd(cmd, 'Error thresholding PVC image',log)
        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time Koo PVC: "+timeprint, log)


    # - Sample to surface
    # ------------------------
    my_print('\nProjecting volumes to surface.\n', log)
    cstime = time.time()
    for hemi in ['lh','rh']:
        my_print('Working with '+hemi+' hemisphere.\n', log)
        my_print('  > GM-MD volumes from EPI space to native Freesurfer surface.\n', log)
        cmd = 'mri_vol2surf --src '+join(dwipreprocpath,'DTI','dti_MD.nii.gz')+' \
                            --out '+join(outputd,'GM-MD',csubject+'.'+hemi+'.nativesurf.GM-MD.'+donsurfversion+'.mgh')+' \
                            --reg '+join(outputd,'reg', 'register.lta')+' \
                            --hemi '+hemi+' \
                            --projfrac 0.5 \
                            --interp nearest \
                            --srchit '+join(outputd,'GM-MD',csubject+'.'+hemi+'.GM-MD.hits.nii.gz')
        run_cmd(cmd, 'Failed to project GM-MD to native space',log)

        my_print('  > SWM-MD volumes from EPI space to native Freesurfer surface.\n', log)
        cmd = 'mri_vol2surf --src '+join(dwipreprocpath,'DTI','dti_MD.nii.gz')+' \
                            --out '+join(outputd,'SWM-MD',csubject+'.'+hemi+'.nativesurf.SWM-MD.'+donsurfversion+'.mgh')+' \
                            --reg '+join(outputd,'reg', 'register.lta')+' \
                            --hemi '+hemi+' \
                            --projdist-avg -3 0 0.1 \
                            --interp nearest \
                            --srchit '+join(outputd,'SWM-MD',csubject+'.'+hemi+'.SWM-MD.hits.nii.gz')
        run_cmd(cmd, 'Failed to project SWM-MD to native space', log)

        if multishell == "True":
            my_print('  > GM-ICVF volumes from EPI space to native Freesurfer surface.\n', log)
            cmd = 'mri_vol2surf --src '+join(dwipreprocpath,'NODDI','FIT_ICVF.nii.gz')+' \
                                --out '+join(outputd,'GM-ICVF',csubject+'.'+hemi+'.nativesurf.GM-ICVF.'+donsurfversion+'.mgh')+' \
                                --reg '+join(outputd,'reg', 'register.lta')+' \
                                --hemi '+hemi+' \
                                --projfrac 0.5 \
                                --interp nearest \
                                --srchit '+join(outputd,'GM-ICVF',csubject+'.'+hemi+'.GM-ICVF.hits.nii.gz')
            run_cmd(cmd, 'Failed to project GM-ICVF to native space', log)

            my_print('  > GM-ISOVF volumes from EPI space to native Freesurfer surface.\n', log)
            cmd = 'mri_vol2surf --src '+join(dwipreprocpath,'NODDI','FIT_ISOVF.nii.gz')+' \
                                --out '+join(outputd,'GM-ISOVF',csubject+'.'+hemi+'.nativesurf.GM-ISOVF.'+donsurfversion+'.mgh')+' \
                                --reg '+join(outputd,'reg', 'register.lta')+' \
                                --hemi '+hemi+' \
                                --projfrac 0.5 \
                                --interp nearest \
                                --srchit '+join(outputd,'GM-ISOVF',csubject+'.'+hemi+'.GM-ISOVF.hits.nii.gz')
            run_cmd(cmd, 'Failed to project GM-ISOVF to native space', log)

            my_print('  > GM-OD volumes from EPI space to native Freesurfer surface.\n', log)
            cmd = 'mri_vol2surf --src '+join(dwipreprocpath,'NODDI','FIT_OD.nii.gz')+' \
                                --out '+join(outputd,'GM-OD',csubject+'.'+hemi+'.nativesurf.GM-OD.'+donsurfversion+'.mgh')+' \
                                --reg '+join(outputd,'reg', 'register.lta')+' \
                                --hemi '+hemi+' \
                                --projfrac 0.5 \
                                --interp nearest \
                                --srchit '+join(outputd,'GM-OD',csubject+'.'+hemi+'.GM-OD.hits.nii.gz')
            run_cmd(cmd, 'Failed to project GM-OD to native space', log)

        else:
            my_print('  > GM-MD PVC volumes from EPI space to native Freesurfer surface.\n', log)
            cmd = 'mri_vol2surf --src '+join(outputd,'GM-MD-koo','dti_MD_koo.nii.gz')+' \
                                --out '+join(outputd,'GM-MD-koo',csubject+'.'+hemi+'.nativesurf.GM-MD-koo.'+donsurfversion+'.mgh')+' \
                                --reg '+join(outputd,'reg', 'register.lta')+' \
                                --hemi '+hemi+' \
                                --projfrac 0.5 \
                                --interp nearest \
                                --srchit '+join(outputd,'GM-MD-koo',csubject+'.'+hemi+'.GM-MD-koo.hits.nii.gz')
            run_cmd(cmd, 'Failed to project PVC GM-MD-koo to native space', log)


        # - Normalize to fsaverage
        # ------------------------
        for metric in metrics:
            my_print('  > '+metric+' values from native surface to fsaverage surface.\n', log)
            cmd = 'mri_surf2surf    --srcsubject '+str(fssubj)+'\
                                    --srcsurfval '+join(outputd,metric,csubject+'.'+hemi+'.nativesurf.'+metric+'.'+donsurfversion+'.mgh')+' \
                                    --trgsubject fsaverage \
                                    --trgsurfval '+join(outputd,metric,csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.mgh')+'\
                                    --hemi '+hemi
            run_cmd(cmd, 'Failed to move '+metric+' values from native to standard surface.', log)
            shutil.copyfile(join(outputd,metric,csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.mgh'),
                            join(fspath,fssubj,'surf', hemi+'.'+metric+'-'+donsurfversion+'.fwhm0.fsaverage.mgh') )

            # - Prepare data for smoothing
            # ---------------------------
            my_print('  > Preparing mask to smooth '+metric+' values.\n', log)
            cmd = 'mri_binarize --i '+join(outputd,metric,csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.mgh')+'\
                                --min 0.00000001 \
                                --o '+join(outputd,metric,'labels','bin_'+csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.mgh')
            run_cmd(cmd,'Failed to generate '+metric+' binarized map.', log)
            cmd = 'mri_cor2label    --i '+join(outputd,metric,'labels','bin_'+csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.mgh')+' \
                                    --l '+join(outputd,metric,'labels','bin_'+csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.label')+'\
                                    --surf fsaverage '+hemi+' \
                                    --id 1'
            run_cmd(cmd,'Failed to generate '+metric+' fsaverage label.' ,log)
            cmd = 'mris_label_calc intersect '+join(outputd,metric,'labels','bin_'+csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.label')+' \
                                            '+join(outputd,metric,'labels',hemi+'.cortex.label')+' \
                                            '+join(outputd,metric,'labels',hemi+'.'+csubject+'_fsaverage.label')
            run_cmd(cmd,'Failed to intersect label crotex and metric one.', log)


            # - Perform smoothing
            # -------------------
            for fwhm in ['10', '15']:
                my_print('  > Smoothing '+metric+' data using FWHM '+fwhm+'.\n', log)
                cmd = 'mri_surf2surf --srcsubject fsaverage \
                                        --srcsurfval '+join(outputd,metric,csubject+'.'+hemi+'.fsaverage.fwhm0.'+metric+'.mgh')+' \
                                        --trgsubject fsaverage \
                                        --trgsurfval '+join(outputd,metric,csubject+'.'+hemi+'.fsaverage.fwhm'+fwhm+'.'+metric+'.mgh')+' \
                                        --hemi '+hemi+' \
                                        --fwhm '+fwhm+' \
                                        --label-trg '+join(outputd,metric,'labels',hemi+'.'+csubject+'_fsaverage.label')
                run_cmd(cmd, 'Failed to smooth '+metric+' surface.', log)

                # - Copy data to Freesurfer folder
                # -------------------
                my_print('  > Copying '+metric+' FWHM='+fwhm+' files into FreeSurfers subject folder.\n', log)
                shutil.copyfile(join(outputd,metric,csubject+'.'+hemi+'.fsaverage.fwhm'+fwhm+'.'+metric+'.mgh'),
                                join(fspath,fssubj,'surf', hemi+'.'+metric+'-'+donsurfversion+'.fwhm'+fwhm+'.fsaverage.mgh') )

    cetime = time.time()
    timeprint = seconds2min(-cstime+cetime)
    my_print(".   Elapsed time surface-projection and smoothing: "+timeprint, log)

    # =========================
    # Clean tmp files
    # =========================

    # =========================
    # Fill .Json file
    # =========================
    globaletime = time.time()
    timeprint = seconds2min(-globtime+globaletime)
    my_print('\n\n > TOTAL ELAPSED TIME: '+timeprint,log)
    cdate = datetime.now()
    outjsondict = { 'DONSURFversion' : donsurfversion ,
                    'DWI-preproc' : dwipreprocversion,
                    'DateInitialProcessing' : str(cdate),
                    'FSversion' : freesurferversion ,
                    'Multi-shell' : multishell,
                    'PVC-method' : 'Koo-2009',
                    'RegistrationMethod' : 'bbr',
                    'Metrics' : metrics,
                    'TotalElapsedTime' : str(timeprint) ,
                    'QC-dwi2t1-reg' : "toCheck"}

    json_object = json.dumps(outjsondict, indent = 4)
    with open(outjson, "w") as outfile:
        outfile.write(json_object)

# ---------------------------
# Run Code
# ---------------------------
if __name__ == "__main__":
    options = options_parser()
    donsurf(options)
