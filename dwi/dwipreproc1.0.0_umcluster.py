#!/usr/bin/env python
# dwipreproc1.0.0_umcluster.py

#
# Pipeline to preprocess DWi data
#
# Original Author: Victor Montal
# Date_first: Feb-14-2022
# Date:

# ---------------------------
# Import Libraries
from __future__ import print_function
import numpy as np
import scipy.io as sio
import argparse
import os
from datetime import datetime
import time
from os.path import join
from os.path import dirname
import json
import time
import shutil
import sys
import subprocess
import logging

import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from nibabel.nifti1 import load
import nibabel as nib
from nibabel.freesurfer import read_geometry

import amico

import pdb

# ---------------------------
# Parser Options
# ---------------------------
HELPTEXT = """

dwipreproc1.0.0_umcluster.py [dev version]

Author:
------
Victor Montal
vmontalb [at] protonmail [dot] cat


>> Script to run basic PREPROCESSING of DWI data <<

STEPS:
------
 0.- Software version logging.
 1.- Denoising [1]
 2.- (if) Suceptibility field map computation (TOP-UP) [2]
 3.- Eddy-current + patient motion (+ susceptibility) correction (EDDY) [3,4]
 4.- QC raw data + corrected data
 5.- Fit DTI tensor
 6.- (if) Fit NODDI model using AMICO implementation [5]


REFS:
-----
[1] Manjon JV, Coupe P, Concha L, Buades A, Collins DL
    "Diffusion Weighted Image Denoising Using Overcomplete Local PCA"
    (2013). PLoS ONE 8(9): e73021. doi:10.1371/journal.pone.0073021.

[2] J.L.R. Andersson, S. Skare, J. Ashburner
    "How to correct susceptibility distortions in spin-echo echo-planar
           images: application to diffusion tensor imaging"
    (2003) NeuroImage, 20(2):870-888

[3] Jesper L. R. Andersson and Stamatios N. Sotiropoulos
    "An integrated approach to correction for off-resonance effects and subject
       movement in diffusion MR imaging"
   (2016) NeuroImage, 125:1063-1078

[4] Jesper L. R. Andersson, Mark S. Graham, Ivana Drobnjak, Hui Zhang and Jon Campbell.
    "Susceptibility-induced distortion that varies due to motion: Correction
           in diffusion MR without acquiring additional data"
   (2018) NeuroImage, 171:277-295

[5] Alessandro Daducci, Erick Canales-Rodriguez, Hui Zhang, Tim Dyrby,
       Daniel Alexander, Jean-Philippe Thiran
   "Accelerated Microstructure Imaging via Convex Optimization (AMICO)
            from diffusion MRI data"
    (2015) NeuroImage 105, pp. 32-44
"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_bidspath  = 'Full path to subject (BIDS) sourcedata dwi data.'
    parser.add_argument('-p',  dest = 'subjectpath', action = 'store', help = h_bidspath, required = True)

    args = parser.parse_args()

    # Check .json file does not exist

    return args


# ---------------------------
# Subfunctions
# ---------------------------
def my_print(message, ff):
    """
    print message, then flush stdout
    """
    print(message)
    ff.write(message+'\n')
    sys.stdout.flush()

def run_cmd(cmd,err_msg,ff):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)

def encoding_info(injson, jsonfile, ff):
    """
    Retrive ReadOut and Phase incoding info from json file
    """

    if 'PhaseEncodingDirection' in injson:
        tmpphase = injson['PhaseEncodingDirection']
    else:
        try:
            tmpphase = injson['PhaseEncodingAxis']
        except KeyError:
            my_print("No field named PhaseEncodingDirection or PhaseEncodingAxis in: "+jsonfile, ff)
    if 'TotalReadoutTime' in injson:
        readouttimes = injson['TotalReadoutTime']
    else:
        try:
            readouttimes = injson['EstimatedTotalReadoutTime']
        except KeyError:
            my_print("No field named TotalReadoutTime or EstimatedTotalReadoutTime in: "+jsonfile, ff)

    if "j" in tmpphase:
        phase_vect = np.array([0,1,0,readouttimes])
    elif "i" in tmpphase:
        phase_vect = np.array([1,0,0,readouttimes])
    else:
        phase_vect = np.array([0,0,1,readouttimes])
    if "-" in tmpphase:
        phase_vect = np.multiply(phase_vect, np.array([-1,-1,-1,1]))
    phase_vect = ' '.join(np.array(phase_vect, dtype = str))

    return phase_vect

def seconds2min(ctime):
    seconds = ctime % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    strtime = "{hour}h: {min}min: {sec}s".format(hour=hour,min=minutes,sec=round(seconds))

    return strtime


# ---------------------------
# Main Code
# ---------------------------
def dwipreproc(options):
    """
    STEPS:
    """
    globtime = time.time()

    # =========================
    # Parse (used) Software versions
    # =========================
    fslv = os.path.basename(dirname(os.environ['FSLDIR']))
    fsv =  os.path.basename(os.environ['FREESURFER_HOME'])
    dipyv = subprocess.check_output("dipy_info --version", shell=True)
    dipyv = dipyv.decode().strip()

    # =========================
    # Define input variables based on BIDS
    # =========================
    dwipreprocversion = "dwipreproc1.0.0"
    freesurferversion = "freesurfer7.1.1"
    cwd = dirname(os.path.realpath(__file__))

    bidspath = dirname( dirname( dirname( dirname(options.subjectpath))))
    derivatives = join(bidspath,'derivatives')

    subj = os.path.basename( dirname( dirname( options.subjectpath)))
    visit = os.path.basename( dirname( options.subjectpath))
    csubject = subj+"_"+visit

    inputd = options.subjectpath
    inputjson = join(inputd,subj+"_"+visit+"_dwi.json")
    inputdwi = join(inputd,subj+"_"+visit+"_dwi.nii")
    inputbvec = join(inputd,subj+"_"+visit+"_dwi.bvec")
    inputbval = join(inputd,subj+"_"+visit+"_dwi.bval")
    outputd = join(derivatives,subj,visit,'dwi',dwipreprocversion)

    # =========================
    # Check subject not running in queue
    # =========================
    outjson = join(outputd,'dwipreproc1.0.0.json')
    if os.path.exists(outjson):
        print("Subject already in Slurm queue")
        exit()


    # =========================
    # Parse DWI input info
    # =========================
    jsonf = open(inputjson,'r')
    datajson = json.load(jsonf)
    jsonf.close()

    # Acqu parameters
    te = datajson['EchoTime']
    te = str( float(te) * 1000)      # In ms for PVC
    tr = datajson['RepetitionTime']
    tr = str( float(tr) * 1000)     # In ms for PVC

    # Check if DWI is multi-shell
    tmpbval = np.loadtxt(inputbval, dtype = int)
    if np.unique(tmpbval[tmpbval > 50]).size > 1:
        multishell = True
    else:
        multishell = False

    # bval
    if multishell:
        bval = np.unique(tmpbval)[1:]  # Skip initial
    else:
        bval = np.max(tmpbval)

    # Num directions
    numdir = np.sum(tmpbval != 0)
    numvols = np.sum(tmpbval > -0.1)

    # Denoise patch size
    if numvols > 124:
        patchsize = 3
    else:
        patchsize = 2


    # Check if inverse is present
    b0inv = join(inputd,subj+"_"+visit+"_B0inv.nii")
    if os.path.exists(b0inv):
        topup = True
    else:
        topup = False
        b0inv = 'None'

    # =========================
    # Generate output structure
    # =========================
    os.makedirs(outputd,exist_ok = True)
    os.makedirs(join(outputd,'QC'), exist_ok = True)
    os.makedirs(join(outputd,'tmp'), exist_ok = True)
    os.makedirs(join(outputd,'EDDY'), exist_ok = True)
    os.makedirs(join(outputd,'DTI'), exist_ok = True)
    if topup:
        os.makedirs(join(outputd,'TOPUP'), exist_ok = True)
    if multishell:
        os.makedirs(join(outputd,'NODDI'), exist_ok = True)

    # ========================
    # LOG software versions and input params
    # =========================
    logfile = join(outputd, 'log_file.log')
    log = open(logfile, 'w')
    logparams = '''\
++++++++++++++++++++++++++++++++++++
+ DWI PREPROCESSING 1.0.0 PIPELINE +
++++++++++++++++++++++++++++++++++++

SOFTWARE VERSION:
-----------------
 · FSL         = {fslversion}
 · DIPY        = {dipyversion}


PIPELINE PARAMETERS:
--------------------
 · Input DWI           : {inputdwi}
 · Input Bvecs         : {bvecfile}
 · Input Bvals         : {bvalfile}
 · Output dir          : {outdir}

 · Denoising           : {denoisemethod}
    · Patch size       : {patchsz}
 · TOPUP               : {topupmethod}
    · BO inverse encod : {B0inv}
 · Eddy current corr   : {eddymethod}
 · B0-T1 registration  : {regmethod}
 · Partial Volume Corr : {pvcmethod}


DWI PARAMETERS:
---------------
 · Num directions  : {numvols}
 · Multi-shell dwi : {multish}
 · B-value         : {bval}
 · TE              : {te}
 · TR              : {tr}

    '''.format(fslversion=fslv ,dipyversion=dipyv ,inputdwi=inputdwi,
                bvecfile=inputbvec ,bvalfile=inputbval,
                outdir=outputd ,denoisemethod='MMPCA' ,regmethod='bbr',
                topupmethod=str(topup) ,B0inv=b0inv ,eddymethod='eddy',
                pvcmethod='Koo-2009' , multish=str(multishell),
                patchsz=str(patchsize), numvols=str(numdir) ,bval=str(bval),
                te=te, tr=tr)
    log.write(logparams)
    sys.stdout.flush()

    # =========================
    # Declare empty .Json
    # =========================
    ## This is needed to avoid running the script twice in the cluster
    outjson = join(outputd,'dwipreproc1.0.0.json')
    open(outjson, 'a').close()

    # =========================
    # DWI preprocessing
    # =========================
    logprint = '''\

++ DWI PREPROCESSING
++++++++++++++++++++++++++
'''
    log.write(logprint)
    sys.stdout.flush()

    # - Denoise image
    # ---------------
    my_print('> Performing DWI denoising..',log)
    cstime = time.time()
    invol = inputdwi
    outvol = join(outputd,subj+"_"+visit+"_dwi-denoise.nii.gz")
    cmd = 'dipy_denoise_mppca '+invol+'\
            --out_dir '+outputd+' \
            --patch_radius '+str(patchsize)+' '+str(patchsize)+' '+str(patchsize)
    run_cmd(cmd,'Denoising using MP-PCA failed.', log)
    os.rename(join(outputd,'dwi_mppca.nii.gz'), outvol)
    cetime = time.time()
    timeprint = seconds2min(-cstime+cetime)
    my_print(".   Elapsed time DENOISING: "+timeprint, log)

    # - Image-distorsion correction
    # -----------------------------
    if topup:
        cstime = time.time()
        my_print('\n> Performing TOPUP field-distorsion correction..',log)
        topupconf = join(outputd,'TOPUP','topup_parameters.txt')

        # Get phase encoding of original DWI
        phase = encoding_info(datajson, inputjson, log)

        # Split b0 images from orig DWI
        invol = join(outputd,subj+"_"+visit+"_dwi-denoise.nii.gz")
        b0pos = np.where(tmpbval == 0)[0]
        concat = ''
        ff = open(topupconf,'w')
        for cb0pos in b0pos:
            concat = join(outputd,'TOPUP','b0-vol'+str(cb0pos)+'.nii.gz')+' '+concat
            cmd = 'mri_convert '+invol+'#'+str(cb0pos)+' \
                    '+join(outputd,'TOPUP','b0-vol'+str(cb0pos)+'.nii.gz')
            run_cmd(cmd,'Error stripping vol'+str(cb0pos)+' B0 volume', log)
            ff.write(phase+"\n")

        # Append phase encoding of inverse-encode DWI to config file
        invb0jsonf = join(inputd,subj+"_"+visit+"_B0inv.json")
        invb0json = open(invb0jsonf,'r')
        invdatajson = json.load(invb0json)
        invb0json.close()

        invphase = encoding_info(invdatajson,invb0jsonf,log)

        ff.write(invphase+"\n")
        ff.close()

        # Concat b0 images for topup
        invb0topup = join(outputd,'TOPUP','b0inv-vol.nii.gz')
        cmd = "mri_convert "+b0inv+"#0 "+invb0topup
        run_cmd(cmd, "Failed to parse inv b0 image", log)

        concat = concat+' '+invb0topup
        outvol = join(outputd,'TOPUP','input_topup.nii.gz')
        cmd = "mri_concat "+concat+" --o "+outvol
        run_cmd(cmd, "Failed to concat b0 volums for topup",log)

        # Clean tmp files
        toremove = "rm "+join(outputd,'TOPUP',"b0*.nii.gz")
        os.system(toremove)

        # Run topup
        invol = join(outputd,'TOPUP','input_topup.nii.gz')
        cmd = "topup    --imain="+invol+" \
                        --datain="+topupconf+" \
                        --config=b02b0.cnf \
                        --out="+join(outputd,'TOPUP',"topup_results")+" \
                        --iout="+join(outputd,'TOPUP',"hifi_b0")
        run_cmd(cmd, "TOPUP command failed", log)
        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time TOPUP: "+timeprint, log)

        # - Brainmask image
        # -----------------
        my_print('\n\n> Computing brain mask..',log)
        cmd = "fslmaths "+join(outputd,'TOPUP',"hifi_b0")+" \
                        -Tmean \
                        "+join(outputd,'TOPUP',"hifi_b0")
        run_cmd(cmd,"Failed to mean topup hifi output image",log)

        cstime = time.time()
        cmd = "bet "+join(outputd,'TOPUP',"hifi_b0")+" \
                    "+join(outputd,"dwi_brain")+" \
                    -f 0.2 -R -m"
        run_cmd(cmd,"Failed to compute DWI brainmask",log)
        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time BRAINMASK: "+timeprint, log)

        # - Run Eddy
        # ----------
        my_print('\n\n> Performing motion+distortion+eddy-current corrections..',log)
        ## Generate index and acq parameters
        idxf = join(outputd,'EDDY','index.txt')
        cmd = 'indx="";for ((i=1; i<='+str(tmpbval.size)+'; i+=1)); do indx="$indx 1"; done;echo $indx > '+idxf
        retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
        eddyconf = join(outputd,'EDDY','topup_parameters.txt')
        shutil.copy(topupconf, eddyconf)

        cstime = time.time()
        invol = join(outputd,subj+"_"+visit+"_dwi-denoise.nii.gz")
        cmd = "eddy --imain="+invol+" \
                    --mask="+join(outputd,"dwi_brain_mask")+" \
                    --acqp="+eddyconf+" \
                    --index="+idxf+" \
                    --bvecs="+inputbvec+" \
                    --bvals="+inputbval+" \
                    --topup="+join(outputd,'TOPUP',"topup_results")+" \
                    --data_is_shelled \
                    --out="+join(outputd,'EDDY','eddy_corrected')
        run_cmd(cmd,"Failed to run EDDY.", log)
        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time EDDY: "+timeprint, log)


    # No inverse PhaseEncoding B0 image
    # ---------------------------------
    else:
        # - Brainmask image
        # -----------------
        my_print('\n\n> Computing brain mask..',log)
        invol = join(outputd,subj+"_"+visit+"_dwi-denoise.nii.gz")
        outvol = join(outputd,"b0_img.nii.gz")
        posb0 = np.where(tmpbval == 0)[0]
        cmd = "mri_convert "+invol+"#"+str(posb0[0])+" "+outvol
        run_cmd(cmd,"Failed to get b0 from DWI input", log)

        cstime = time.time()
        cmd = "bet "+join(outputd,"b0_img")+" \
                    "+join(outputd,"dwi_brain")+" \
                    -f 0.2 -R -m"
        run_cmd(cmd,"Failed to compute DWI brainmask",log)
        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time BRAINMASK: "+timeprint, log)


        # - Run Eddy
        # ----------
        my_print('\n\n> Performing motion+eddy-current corrections..',log)
        cstime = time.time()
        # Generate acq params file
        eddyconf = join(outputd,'EDDY','topup_parameters.txt')
        phase = encoding_info(datajson, inputjson, log)

        ff = open(eddyconf,'w')
        ff.write(phase+"\n")
        ff.close()

        # Generate index parameters
        idxf = join(outputd,'EDDY','index.txt')
        cmd = 'indx="";for ((i=1; i<='+str(tmpbval.size)+'; i+=1)); do indx="$indx 1"; done;echo $indx > '+idxf
        retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
        # Run Eddy
        invol = join(outputd,subj+"_"+visit+"_dwi-denoise.nii.gz")
        cmd = "eddy --imain="+invol+" \
                    --mask="+join(outputd,"dwi_brain_mask")+" \
                    --acqp="+eddyconf+" \
                    --index="+idxf+" \
                    --bvecs="+inputbvec+" \
                    --bvals="+inputbval+" \
                    --data_is_shelled \
                    --out="+join(outputd,'EDDY','eddy_corrected')
        run_cmd(cmd,"Failed to run EDDY.", log)
        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time EDDY: "+timeprint, log)

    # Run (and parse) QC
    # ---------------------------------
    my_print('\n\n> Generating EDDY QC log..',log)
    cstime = time.time()
    cmd = "eddy_quad "+join(outputd,'EDDY','eddy_corrected')+" \
                        -idx "+idxf+" \
                        -par "+eddyconf+" \
                        -m "+join(outputd,"dwi_brain_mask")+" \
                        -b "+inputbval
    run_cmd(cmd," Error computing eddyQC log..", log)
    os.rename(join(outputd,'EDDY','eddy_corrected.nii.gz'),
                join(outputd,subj+"_"+visit+"_dwi-eddy-corr.nii.gz"))
    os.rename(join(outputd,'EDDY','eddy_corrected.eddy_rotated_bvecs'),
                join(outputd,subj+"_"+visit+"_dwi_rot.bvec"))
    shutil.copy(join(outputd,'EDDY','eddy_corrected.qc','qc.pdf'),
                join(outputd,'QC','dwi-eddy-qc.pdf'))
    cetime = time.time()
    timeprint = seconds2min(-cstime+cetime)
    my_print(".   Elapsed time eddyQC: "+timeprint, log)

    # Model fitting
    # -------------
    # DTI
    my_print('\n\n> Fitting DTI tensor from DWI data..',log)
    cstime = time.time()
    cmd = 'dtifit   -k '+join(outputd,subj+"_"+visit+"_dwi-eddy-corr.nii.gz")+' \
                    -o '+join(outputd,'DTI','dti')+' \
                    -m '+join(outputd,"dwi_brain_mask")+' \
                    -r '+join(outputd,subj+"_"+visit+"_dwi_rot.bvec")+' \
                    -b '+inputbval
    run_cmd(cmd,'Failed to compute DTI tensor.', log)
    cmd = 'fslmaths '+join(outputd,'DTI','dti_L1')+' -add ' +join(outputd,'DTI','dti_L2')+' -div 2 '+join(outputd,'DTI','dti_RX')
    run_cmd(cmd,'Failed to compute Rx volume.', log)
    cetime = time.time()
    timeprint = seconds2min(-cstime+cetime)
    my_print(".   Elapsed time DTI tensor fitting: "+timeprint, log)

    if multishell:
        # NODDI
        cstime = time.time()
        my_print('\n\n> Fitting AMICO-NODDI model from DWI data..',log)
        # Parse subj path
        ae = amico.Evaluation(study_path=join(derivatives,subj,visit,'dwi'),
                                subject="dwipreproc1.0.0",
                                output_path=join(outputd,'NODDI'))
        # Convert to .scheme from FSL
        shutil.copy(inputbval, join(outputd,subj+"_"+visit+"_dwi.bval"))
        amico.util.fsl2scheme(join(outputd,subj+"_"+visit+"_dwi.bval"),
                                join(outputd,subj+"_"+visit+"_dwi_rot.bvec"),
                                schemeFilename=join(outputd,'NODDI','NODDI_protocol.scheme'))
        shutil.copy(join(outputd,'NODDI','NODDI_protocol.scheme'), join(outputd,'NODDI_protocol.scheme'))
        # Load data
        ae.load_data(dwi_filename = subj+"_"+visit+"_dwi-eddy-corr.nii.gz",
                        scheme_filename = "NODDI_protocol.scheme",
                        mask_filename = "dwi_brain_mask.nii.gz",
                        b0_thr = 10)
        # Compute response functions
        ae.set_model("NODDI")
        ae.generate_kernels()
        ae.load_kernels()
        # Fit AMICO-NODDI
        ae.fit()
        # Save fit
        ae.save_results()

        shutil.rmtree(join(derivatives,subj,visit,'dwi','kernels'))

        cetime = time.time()
        timeprint = seconds2min(-cstime+cetime)
        my_print(".   Elapsed time AMICO-NODDI fitting: "+timeprint, log)


    # =========================
    # Fill .Json file
    # =========================
    globaletime = time.time()
    timeprint = seconds2min(-globtime+globaletime)
    my_print('\n\n > TOTAL ELAPSED TIME: '+timeprint,log)
    cdate = datetime.now()
    outjsondict = { 'DWIpreprocversion' : dwipreprocversion ,
                    'DateInitialProcessing' : str(cdate),
                    'FSLversion' : fslv,
                    'DIPY-version' : dipyv, 'Input-DWI' : inputdwi,
                    'BVec' : inputbvec, 'Bval' : inputbval,
                    'MultiShell' : str(multishell),
                    'Outdir' : outputd,
                    'DenoiseMethod' : 'MMPCA', 'TOPUP' : str(topup),
                    'B0inv' : str(b0inv), 'Eddy-Method' : 'eddy',
                    'NumberVols' : str(numdir), 'Bvalue' : str(bval),
                    'EchoTime(ms)' : str(te), 'RepetitionTime(ms)' : str(tr),
                    'TotalElapsedTime' : str(timeprint) ,
                    'QC-dwi-data' : "toCheck", 'QC-dwi2t1-reg' : "toCheck"}

    json_object = json.dumps(outjsondict, indent = 4)
    with open(outjson, "w") as outfile:
        outfile.write(json_object)


if __name__ == "__main__":
    options = options_parser()
    dwipreproc(options)
