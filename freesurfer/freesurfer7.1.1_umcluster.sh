#!/bin/bash
#freesurfer71_umcluster.sh
##############################
#
# This script will batch into queue a freesurfer job.
#
#############################

function Usage {
    cat <<USAGE
Usage:
`basename $0` -p path_fold_session
Compulsory arguments:
     -p:  path_fold_session : Name of subject.

---------------------------------
V.M.

Memory Unit, Department of Neurology,
Hospital de la Santa Creu i Sant Pau
Biomedical Research Institute Sant Pau
Universitat Autònoma de Barcelona

--------------------------------
USAGE
    exit 1
}

# -- Reading command line arguments
while getopts "p:" OPT
  do
  case $OPT in
      p)  # subject ID
      SUBJSESSION_PATH=$OPTARG
    ;;
      \?) # getopts issues an error message
   echo "$USAGE" >&2
   exit 1
   ;;
  esac
done


# -- Preparing input VARS
FSversion='7.1.1'

VISIT=$(basename ${SUBJSESSION_PATH})
SUBJ=$(basename $(dirname ${SUBJSESSION_PATH}))
SOURCEDATA=$(basename $(dirname $(dirname ${SUBJSESSION_PATH})))
BIDSPATH=$(dirname $(dirname $(dirname ${SUBJSESSION_PATH})))
FSname="freesurfer-${SUBJ}_${VISIT}_v${FSversion}"
T1name="${SUBJ}_${VISIT}_T1w.nii"

INFILE=${BIDSPATH}/${SOURCEDATA}/${SUBJ}/${VISIT}/anat/${T1name} # T1 file input to process
SUBJECTID=${FSname}       # Output subject ID name
OUTPATH=${BIDSPATH}/derivatives/${SUBJ}/${VISIT}/anat/freesurfer${FSversion}  # Output Path
SUBJECTS_DIR=${OUTPATH}    # Needed for freesurfer.

# -- Check running freesurfer version is the correct
activeFS=$(cat ${FREESURFER_HOME}/build-stamp.txt | grep ${FSversion})
if [ -z "${activeFS}" ];
then
  echo "Missmatching Freesurfer Version. Expected ${FSversion}. Found $(cat ${FREESURFER_HOME}/build-stamp.txt)"
  exit 1;
fi

# -- Prepare output folder-structure (if not existing)
mkdir -p ${OUTPATH}

# -- Check subject already in queue to run
if [ ! -f "${OUTPATH}/freesurfer${FSversion}.json" ];
then

  # -- Init .JSON file (if exists, exit)
  touch ${OUTPATH}/freesurfer${FSversion}.json

  # -- Run Freesurfer
  CMD="recon-all -i ${INFILE} -s ${SUBJECTID} -sd ${SUBJECTS_DIR} -all"
  ${CMD}

  # -- Write JSON to output
  cat <<JSON > ${OUTPATH}/freesurfer${FSversion}.json
  {
    "FreesurferVersion": "$(cat ${FREESURFER_HOME}/build-stamp.txt)",
    "DateInitialProcessing": "$(date)",
    "CortexQC": "toReview",
    "NotesQcCortex": "-",
    "LastReviewCortex": "-",
    "SubcorticalQC": "toReview",
    "NotesQcSubcortical": "-",
    "LastReviewSubcortical": "-",
    "BrainstemSegQC": "toReview",
    "NotesQcBrainstem": "-",
    "LastReviewBrainstem": "-",
    "VisualQC": "toReview",
    "LastUserVisualQC": "-",
    "HistoryVisualQC": "-",
    "HistoryUserVisualQC": "-"
  }
JSON
fi


##################
# Script ends here
##################

