#!/usr/bin/env python
# changepoint_plot_scatter_ROI.py

#
# Script to plot the scatterplot for a given ROI in comparison to a time variable
#
# Original Author: Victor Montal
# Date_first: April-10-2019
# Date:

# ---------------------------
# Import Libraries
from __future__ import print_function
import argparse
import os
from os.path import join
import subprocess
import sys
import time
import tempfile
import logging

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import nibabel.freesurfer.mghformat as nibmgh
import nibabel.freesurfer.io as nib

import pdb


# ---------------------------
# Parser Options
HELPTEXT = """

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat


"""

USAGE = """

"""
def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_table     = 'Table containing fsid, time-dependent data (EYO). Second column MUST BE the time-dependent variable (e.g EYO)'
    h_vrtx      = 'Vertex Number from where you want to plot the data.'
    h_fsdir     = "Path to processed Freesurfer subjects"
    h_metric    = "Metric to use to plot the data (e.g thickness,, Wscore-GM-MD, etc)"
    h_hemi      = "Hemisphere. Should be lh or rh"
    h_chpoints  = 'MGH file containing the changepoints (in fsaverage space)'
    h_groups    = 'Plot different points by a group. MUST be a continuous variable. Always THIRD column'

    parser.add_argument('--table',      dest = 'table',     action = 'store',       help = h_table,    required = True)
    parser.add_argument('--fsdir',      dest = 'fsdir',     action = 'store',       help = h_fsdir,     required = True)
    parser.add_argument('--metric',     dest = 'metric',    action = 'store',       help = h_metric,    required = True)
    parser.add_argument('--hemi',       dest = 'hemi',      action = 'store',       help = h_hemi,      required = True)
    parser.add_argument('--vertex1',    dest = 'vertex1',   action = 'store',       help = h_vrtx,      required = True)
    parser.add_argument('--vertex2',    dest = 'vertex2',   action = 'store',       help = h_vrtx,      required = True)
    parser.add_argument('--chpoints',   dest = 'chpoints',  action = 'store',       help = h_chpoints,  required = True)
    parser.add_argument('--groups',     dest = 'groups',    action = 'store_true',  help = h_groups,    required = False, default=False)

    args = parser.parse_args()
    # Check inputs <---------------------------- FreeSurfer 6 plz. && fsaverage in SUBEJCTS_DIR
    fshome = os.environ['FREESURFER_HOME']
    with open(join(fshome, 'build-stamp.txt'), 'r') as f:
        version = f.read()
        if 'stable-pub-v6' not in version:
            print('----------------------------------------------------------------------------------------------------------------')
            print('WARNING: YOU ARE NOT USING FREESURFER VERSION 6. THIS WILL CAUSE ERRORS DURING THE PROCESSING I.E DILATATION LABEL.')
            print('----------------------------------------------------------------------------------------------------------------')
            time.sleep(10)

    return args


# ---------------------------
# Subfunctions
def my_print(message):
    """
    print message, then flush stdout
    """
    print(message)
    sys.stdout.flush()

def run_cmd(cmd,err_msg):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n')
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg)
        sys.exit(1)
    my_print('\n')

def check_req():

    return 1


# ---------------------------
# Main Code
def scatter_roi(options):
    # Load Data
    my_print("Preparing scatterplot data...")
    my_print(". Loading time-dependant data")
    glmraw = pd.read_csv(options.table, sep="\t")
    allfs = glmraw.iloc[:,0].tolist()

    my_print('. Checking required files exists \n')
    fsfiles = [ os.path.join(options.fsdir,subj,'surf',options.hemi+'.'+options.metric+'.fwhm15.fsaverage.mgh') for subj in allfs ]
    for subj in fsfiles:
        if not os.path.exists(subj):
            my_print('File '+subj+' does not exist in data dir.')
            raise SystemExit()

    my_print('. Concatenating input files.\n')
    concatfiles = ' '.join(fsfiles)
    cmd = 'mri_concat   --i '+concatfiles+' \
                        --o '+join('/tmp',options.hemi+'.tmp_concat.mgh')
    run_cmd(cmd, "Error while concatenating input volumes")


    # Create Label and dilate it
    my_print('. Creating label for first vertex \n')
    tmp = tempfile.NamedTemporaryFile(delete=False, mode='w+t')
    tmp.writelines("#!ascii label  , from subject fsaverage vox2ras=TkReg\n")
    tmp.writelines("1\n")
    tmp.writelines(options.vertex1+" -27.106  -80.730  11.969 0.0000000000\n")
    tmp.close()

    my_print('. Dilateting the first single-vertex value with a radius 5 \n')
    cmd="mri_label2label --srclabel "+tmp.name+" \
                            --dilate 3 \
                            --s fsaverage --hemi "+options.hemi+" \
                            --trglabel "+tmp.name+"_dilated \
                            --regmethod surface"
    run_cmd(cmd,"Error dilateting the label")
    my_print(".     Dilated label of first vertex can be find at: "+str(tmp.name+"_dilated.label \n\n"))

    my_print('. Creating label for second vertex \n')
    tmp2 = tempfile.NamedTemporaryFile(delete=False, mode='w+t')
    tmp2.writelines("#!ascii label  , from subject fsaverage vox2ras=TkReg\n")
    tmp2.writelines("1\n")
    tmp2.writelines(options.vertex2+" -27.106  -80.730  11.969 0.0000000000\n")
    tmp2.close()

    my_print('. Dilateting the first single-vertex value with a radius 5 \n')
    cmd="mri_label2label --srclabel "+tmp2.name+" \
                            --dilate 3 \
                            --s fsaverage --hemi "+options.hemi+" \
                            --trglabel "+tmp2.name+"_dilated \
                            --regmethod surface"
    run_cmd(cmd,"Error dilateting the label")
    my_print(".     Dilated label can be find at: "+str(tmp2.name+"_dilated.label \n\n"))


    # Get data (mean inside label)
    my_print(".  Preparing data for plotting \n")
    dat = nibmgh.load(join('/tmp',options.hemi+'.tmp_concat.mgh'))
    dat = dat.get_data()
    datshape = dat.shape
    datrs = dat.reshape(datshape[0], -1)

    chpoint = nibmgh.load(options.chpoints)
    chpoint = chpoint.get_data()
    chpointshape = chpoint.shape
    chpointrs = chpoint.reshape(chpointshape[0], -1)

    labelidx1 = nib.read_label(tmp.name+"_dilated.label")
    labelvals1 = np.zeros([labelidx1.shape[0],datrs.shape[1]])
    labelvals1 = datrs[labelidx1,:]
    labelvals1[labelvals1 == 0] = np.nan

    labelidx2 = nib.read_label(tmp2.name+"_dilated.label")
    labelvals2 = np.zeros([labelidx2.shape[0],datrs.shape[1]])
    labelvals2 = datrs[labelidx2,:]
    labelvals2[labelvals2 == 0] = np.nan

    # Final data to plot
    avgmetric1 = np.nanmean(labelvals1, axis=0)
    tdependent = glmraw.ix[:,1].tolist()
    fchangepoint1 = np.mean(chpointrs[labelidx1])
    z1 = np.polyfit(tdependent, avgmetric1, 2)
    zplot1 = np.poly1d(z1)
    zplotaxis1 = np.linspace(min(tdependent),max(tdependent),len(tdependent))

    avgmetric2 = np.nanmean(labelvals2, axis=0)
    tdependent = glmraw.ix[:,1].tolist()
    fchangepoint2 = np.mean(chpointrs[labelidx2])
    z2 = np.polyfit(tdependent, avgmetric2,2)
    zplot2 = np.poly1d(z2)
    zplotaxis2 = np.linspace(min(tdependent),max(tdependent),len(tdependent))

    # Plot
    my_print("Ploting scatterplot between time-dependant variable and "+options.metric)
    my_print(". Using the dilated region around the vertex: "+options.vertex1 + " and "+options.vertex2)
    my_print(". Plotting the inflexion point(s) at: "+str(fchangepoint1)+" and "+str(fchangepoint2))
    fig,ax = plt.subplots()

    plt.scatter(tdependent,avgmetric1, c='r',marker='o')
    ax.plot(zplotaxis1, zplot1(zplotaxis1), color='r', linestyle='-')
    ax.axvline(x=fchangepoint1, color='r', linestyle='--')

    plt.scatter(tdependent,avgmetric2, c='b',marker='s')
    ax.plot(zplotaxis2, zplot2(zplotaxis2), color='b', linestyle='-')
    ax.axvline(x=fchangepoint2, color='b', linestyle='--')


    ax.axhline(y=0,color='k', linestyle='--')
    plt.show()


if __name__ == "__main__":
    options = options_parser()
    scatter_roi(options)
