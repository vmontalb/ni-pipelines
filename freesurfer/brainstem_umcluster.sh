#!/bin/bash
#freesurfer_umcluster.sh
##############################
#
# This script will run the Freesurfer 7.1 BrainStem Segmentation.
#
#############################

function Usage {
    cat <<USAGE
Usage:
`basename $0` -s subjectID -d subjDir
Compulsory arguments:
     -s:  subjectID : Name of freesurfer-folder.
     -d:  subjDir : path/where/FreesurferFolder/is/located. Freesurfers SUBJECTS_DIR


---------------------------------
V.M.

Memory Unit, Department of Neurology,
Hospital de la Santa Creu i Sant Pau
Biomedical Research Institute Sant Pau
Universitat Autònoma de Barcelona

--------------------------------
USAGE
    exit 1
}

# Provide output for Help
if [[ "$1" == "-h" || $# -eq 0 ]];
  then
    Usage >&2
  fi

# Reading command line arguments
while getopts "s:d:" OPT
  do
  case $OPT in
      s)  # subject ID
      SUBJECTID=$OPTARG
    ;;
      d)  # input nifti file
      export SUBJECTS_DIR=$OPTARG
    ;;
      \?) # getopts issues an error message
   echo "$USAGE" >&2
   exit 1
   ;;
  esac
done


# Run BrainStem segmentation
CMD="segmentBS.sh ${SUBJECTID} ${SUBJECTS_DIR} "
${CMD}


##################
# Script ends here
##################
