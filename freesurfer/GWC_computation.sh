#!/bin/bash
source ~/Documentos/FREESURFER/common_lib.sh
#GWC.sh

##############################
#
# This script computes the Gray matter / White matter contrast values.
#
#############################

function Usage {
    cat <<USAGE
Description:
 > This script computes the Gray matter / White matter contrast values for a list of subjects. <

Usage:
`basename $0` -d subjectsDir -e editsType
Compulsory arguments:
     -l:  subjectsList: path/to/txt file with the subjects to re-process.
     -f: FreeSurferDir: path/to/FS-dir
     -o: Output path.
     -n: Intensity normalization type (T1 (Fs T1.mgz) or brain (Fs brain.mgz) or orig (orig.mgz) or nu (nu.mgz) or ANTS (using N4 bias correction))
     -c: Boolean for just fetching the GWC volumes to FS subjects. (true or false)

---------------------------------
V.M.

Memory Unit, Department of Neurology,
Hospital de la Santa Creu i Sant Pau
Biomedical Research Institute Sant Pau
Universitat Autònoma de Barcelona

--------------------------------
USAGE
    exit 1
}


# Provide output for Help
if [[ "$1" == "-h" || $# -eq 0 ]];
  then
    Usage >&2
  fi


# ----------------------------------------------------
# Reading command line arguments
while getopts "h:l:n:c:f:o:" OPT
    do
    case $OPT in
        h) #help
     Usage
     exit 0
     ;;
        l)  # Subjects dir. Path
     SUBJECTS_LIST=$OPTARG
     ;;
        f)  # Subjects dir. Path
     export SUBJECTS_DIR=$OPTARG
     ;;
        n)  # Type normalization (T1,brain,ants)
     NORM=$OPTARG
     ;;
        c)  # Fetching boolean
     FETCH=$OPTARG
     ;;
        o)  # Output path
     OUT_PATH=$OPTARG
     ;;
        \?) # getopts issues an error message
     echo "$USAGE" >&2
     exit 1
     ;;
    esac
 done

# ----------------------------------------------------
# Sanitaze variables

if [[ "$NORM" == "T1" || "$NORM" == "brain" || "$NORM" == "ANTS" || "$NORM" == "orig" || "$NORM" == "nu" ]]; then
    echo "Correct normalization type."
else
    echo "ERROR: Normalization type must be T1, BRAIN or ANTS !! "
    exit 1
fi

if [[ "$FETCH" == "true" || "$FETCH" == "false" ]]; then
    echo "Correct fetching arg."
else
    echo "ERROR: Incorrect fetching parameter !! It must be true (for just fetching) or false (all pipeline)"
    exit 1
fi

# ----------------------------------------------------
# Save running cmd to a txt
echo "$0 $@" > ${OUT_PATH}/cmd.log

# ----------------------------------------------------
# Main Code
cat $SUBJECTS_LIST | while read g;
do
    LOG "Working with subject $g"
    STARTTIME=$(date +"%s")
    currdir=${OUT_PATH}/${g}
    mkdir $currdir

    subjfs=$(ls ${SUBJECTS_DIR} | grep ${g})

    if [[ $FETCH == "false" ]];
    then

        if [ "$NORM" == "ANTS" ]; then
            LOG "   Normalizing T1w intensity w/ ANTs N4 ..."
            # Fetch data
            ln -s $SUBJECTS_DIR/$subjfs/mri/orig.mgz $currdir/orig.mgz


            # N4BiasFieldCorrection (DOI: 10.1109/TMI.2010.2046908)
            cmd="N4BiasFieldCorrection  -d 3 -i $currdir/orig.mgz \
                                        -o $currdir/t1_n4biascorrected.mgz
                                        --verbose 1"
            echo $cmd
            ${cmd}
            NORMf=$currdir/t1_n4biascorrected.mgz

        else
            LOG "   Using $NORM volume as normalized T1w data ..."
            # Fetch data
            ln -s $SUBJECTS_DIR/$subjfs/mri/$NORM.mgz $currdir/${NORM}.mgz
            NORMf=$currdir/${NORM}.mgz
        fi

        # Compute GM (proj 0.5 from Thickness)
        LOG "   Computing the GM value at 0.5 proj distance ..."
        for hemi in lh rh; do
            cmd="mri_vol2surf   --mov ${NORMf} --regheader $subjfs \
                                --hemi $hemi --projfrac 0.5 \
                                --o $currdir/$hemi.GM-0.5.native.mgh \
                                --srchits $currdir/$hemi.hits.GM-0.5.native.mgh"
            echo $cmd
            ${cmd}
        done

        # Compute WM (1mm under wm surface)
        LOG "   Computing the WM value at -1mm from WM surface ..."
        for hemi in lh rh; do
            cmd="mri_vol2surf   --mov ${NORMf} --regheader $subjfs \
                                --hemi $hemi --projdist -1 \
                                --o $currdir/$hemi.WM-1mm.native.mgh \
                                --srchits $currdir/$hemi.hits.WM-1mm.native.mgh"
            echo $cmd
            ${cmd}
        done

        # Compute GWC
        LOG "   Computing the GWC value ...."
        for hemi in lh rh; do
            # WM - GM
            cmd="mris_calc -o $currdir/$hemi.tmp1.mgh $currdir/$hemi.WM-1mm.native.mgh sub $currdir/$hemi.GM-0.5.native.mgh "
            echo $cmd
            ${cmd}
            # WM + GM
            cmd="mris_calc -o $currdir/$hemi.tmp2.mgh $currdir/$hemi.WM-1mm.native.mgh add $currdir/$hemi.GM-0.5.native.mgh "
            echo $cmd
            ${cmd}
            # Ratio
            cmd="mris_calc -o $currdir/$hemi.tmp3.mgh $currdir/$hemi.tmp1.mgh div $currdir/$hemi.tmp2.mgh"
            echo $cmd
            ${cmd}
            # Percentage
            cmd="mris_calc -o  $currdir/$hemi.${g}.GWC.native.mgh $currdir/$hemi.tmp3.mgh mul 100"
            echo ${cmd}
            ${cmd}

        done

        # Move to fsaverage
        LOG "   Moving the GWC map to fsaverage space ..."
        for hemi in lh rh; do
            cmd="mri_surf2surf  --srcsubject ${subjfs} --srcsurfval $currdir/$hemi.${g}.GWC.native.mgh \
                                --trgsubject fsaverage --trgsurfval $currdir/$hemi.${g}.GWC.fsaverage.fwhm0.mgh \
                                --hemi $hemi"
            echo $cmd
            ${cmd}
        done

        # Smooth data
        LOG "   Smoothing GWC w/ FWHM=10 ..."
        for hemi in lh rh; do
            cmd="mri_surf2surf  --srcsubject fsaverage --srcsurfval $currdir/$hemi.${g}.GWC.fsaverage.fwhm0.mgh \
                                --trgsubject fsaverage --trgsurfval $currdir/$hemi.${g}.GWC.fsaverage.fwhm10.mgh \
                                --hemi lh --fwhm 10 --cortex"
            echo $cmd
            ${cmd}
        done

        LOG "   Smoothing GWC w/ FWHM=15 ..."
        for hemi in lh rh; do
            cmd="mri_surf2surf  --srcsubject fsaverage --srcsurfval $currdir/$hemi.${g}.GWC.fsaverage.fwhm0.mgh \
                                --trgsubject fsaverage --trgsurfval $currdir/$hemi.${g}.GWC.fsaverage.fwhm15.mgh \
                                --hemi lh --fwhm 15 --cortex"
            echo $cmd
            ${cmd}
        done


        # Remove tmp files
        #for hemi in lh rh; do
        #    rm $currdir/$hemi.tmp1.mgh
            #rm $currdir/$hemi.tmp2.mgh
            #rm $currdir/$hemi.tmp3.mgh
        #done
    fi

    # Fetching files to FS folder
    LOG "   Copying files to FreeSurfer subjec foler (4 qdec) ..."
    for hemi in lh rh; do
        cp $currdir/$hemi.${g}.GWC.fsaverage.fwhm10.mgh $SUBJECTS_DIR/$subjfs/surf/$hemi.GWC.fwhm10.fsaverage.mgh
        cp $currdir/$hemi.${g}.GWC.fsaverage.fwhm15.mgh $SUBJECTS_DIR/$subjfs/surf/$hemi.GWC.fwhm15.fsaverage.mgh
    done

    ENDTIME=$(date +"%s")
    ELAPSED=$((${ENDTIME} - ${STARTTIME}))
    LOG "   TIME ELAPSED FOR SUBJECT ${g} is $((${ELAPSED} / 60)) minutes and $((${ELAPSED} % 60)) seconds."

done





  LOG "---------"
  LOG "  DONE"
  LOG "---------"
