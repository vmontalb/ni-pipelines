#!/usr/bin/env python
# qc_freesurfer_samwise.py

#
# Compute QC measures from Freesurfer segmentation
# Also compute screenshots of the segmentation
#
# Original Author: Victor Montal
# Date_first: March-31-2022
# Date:

# ---------------------------
# Import Libraries
# ---------------------------
import os
from os.path import join
import time
import argparse

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from nibabel.nifti1 import load
import nibabel as nib
from nibabel.freesurfer import read_geometry

from qatoolspython import qatoolspython

# ---------------------------
# Parser Options
# ---------------------------
HELPTEXT = """

qc_freesurfer_samwise.py.py [dev version]

Author:
------
Victor Montal
vmontalb [at] protonmail [dot] cat


>> Script to Compute QC data and screenshots of Freesurfer segmentation <<

STEPS:
------
 1.- Compute QC metrics using qatools [1]
 2.- Compute screenshots

REFS:
-----
[1] https://github.com/deep-mi/qatools-python

"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_fspath  = 'Full path to Freesurfer subject.'
    parser.add_argument('-p',  dest = 'subjectpath', action = 'store', help = h_fspath, required = True)

    args = parser.parse_args()

    return args

# ---------------------------
# Main Code
# ---------------------------
def qc_freesurfer(options):
    """
    """
    # Parse info and priors
    cpath = options.subjectpath
    outpath = join(cpath,'QC')

    lhpial = join(cpath,'surf','lh.pial')
    rhpial = join(cpath,'surf','rh.pial')
    lhwhite = join(cpath,'surf','lh.white')
    rhwhite = join(cpath,'surf','rh.white')
    invol = join(cpath,"mri","brainmask.mgz")

    # Create directory structure
    os.makedirs(join(outpath,'screenshots'), exist_ok=True)

    # Compute QC metrics using qatools
    fspath = os.path.dirname(cpath)
    subjname = os.path.basename(cpath)

    qatoolspython.run_qatools(subjects_dir=fspath,
                                output_dir=outpath,
                                subjects=[subjname],
                                shape=False)
                                
    
    # Load MRI data for screenshots
    mrivol = join(cpath,'mri','T1.mgz')
    mri = nib.load(mrivol)
    mridata = mri.get_fdata()

    lhcoordp, lhfacep = read_geometry(lhpial)
    rhcoordp, rhfacep = read_geometry(rhpial)
    lhcoordw, lhfacew = read_geometry(lhwhite)
    rhcoordw, rhfacew = read_geometry(rhwhite)

    # Transform surface pos to plot correctly
    Torig = mri.header.get_vox2ras_tkr()
    invTorig = np.linalg.inv(Torig)
    lhvoxp = np.dot(lhcoordp, invTorig[:3, :3].T)  # Apply transform
    lhvoxp += invTorig[:3, 3] # Apply translation
    rhvoxp = np.dot(rhcoordp, invTorig[:3, :3].T)  # Apply transform
    rhvoxp += invTorig[:3, 3] # Apply translation

    lhvoxw = np.dot(lhcoordw, invTorig[:3, :3].T)  # Apply transform
    lhvoxw += invTorig[:3, 3] # Apply translation
    rhvoxw = np.dot(rhcoordw, invTorig[:3, :3].T)  # Apply transform
    rhvoxw += invTorig[:3, 3] # Apply translation

    vox = np.vstack((lhvoxp,rhvoxp,lhvoxw,rhvoxw))
    tris = np.vstack((lhfacep,np.max(lhfacep) + 1 +rhfacep))
    tris = np.vstack((tris,np.max(tris) + 1 +lhfacew))
    tris = np.vstack((tris,np.max(tris) + 1 +rhfacew))


    # Perfom sagittal screenshots
    voxposvis1 = [60,70,80,90]
    voxposvis2 = [100,110,120,130]
    voxposvis3 = [140,150,160,170]
    voxposvis4 = [180,190,200,210]
    fig, ax = plt.subplots(4, 4, figsize=(30,30))
    fig.set_tight_layout({'pad': 0})
    fig.subplots_adjust(wspace=0)

    # View-1
    for idx,cpos in enumerate(voxposvis1):
        toplot = mridata[cpos,:,:]
        ax[0,idx].imshow(toplot, cmap='gray') # MRI image
        ax[0,idx].axis('off')
        ax[0,idx].tricontour(vox[:, 2], vox[:, 1], tris, vox[:, 0],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[0,idx].set_aspect('equal')

    # View-2
    for idx,cpos in enumerate(voxposvis2):
        toplot = mridata[cpos,:,:]
        ax[1,idx].imshow(toplot, cmap='gray') # MRI image
        ax[1,idx].axis('off')
        ax[1,idx].tricontour(vox[:, 2], vox[:, 1], tris, vox[:, 0],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[1,idx].set_aspect('equal')

    # View-3
    for idx,cpos in enumerate(voxposvis3):
        toplot = mridata[cpos,:,:]
        ax[2,idx].imshow(toplot, cmap='gray') # MRI image
        ax[2,idx].axis('off')
        ax[2,idx].tricontour(vox[:, 2], vox[:, 1], tris, vox[:, 0],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[2,idx].set_aspect('equal')

    # View-4
    for idx,cpos in enumerate(voxposvis4):
        toplot = mridata[cpos,:,:]
        ax[3,idx].imshow(toplot, cmap='gray') # MRI image
        ax[3,idx].axis('off')
        ax[3,idx].tricontour(vox[:, 2], vox[:, 1], tris, vox[:, 0],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[3,idx].set_aspect('equal')

    outname = 'sagittal_'+subjname+'.png'
    fig.savefig(join(outpath,'screenshots',outname))

    # Perfom coronal screenshots
    voxposvis1 = [60,70,80,90]
    voxposvis2 = [100,110,120,130]
    voxposvis3 = [140,150,160,170]
    voxposvis4 = [180,190,200,210]
    fig, ax = plt.subplots(4, 4, figsize=(30,30))
    fig.set_tight_layout({'pad': 0})
    fig.subplots_adjust(wspace=0)

    # View-1
    for idx,cpos in enumerate(voxposvis1):
        toplot = mridata[:,:,cpos]
        toplot = toplot.swapaxes(0,1)
        ax[0,idx].imshow(toplot, cmap='gray') # MRI image
        ax[0,idx].axis('off')
        ax[0,idx].tricontour(vox[:, 0], vox[:, 1] , tris, vox[:, 2],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[0,idx].set_aspect('equal')

    # View-2
    for idx,cpos in enumerate(voxposvis2):
        toplot = mridata[:,:,cpos]
        toplot = toplot.swapaxes(0,1)
        ax[1,idx].imshow(toplot, cmap='gray') # MRI image
        ax[1,idx].axis('off')
        ax[1,idx].tricontour(vox[:, 0], vox[:, 1] , tris, vox[:, 2],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[1,idx].set_aspect('equal')

    # View-3
    for idx,cpos in enumerate(voxposvis3):
        toplot = mridata[:,:,cpos]
        toplot = toplot.swapaxes(0,1)
        ax[2,idx].imshow(toplot, cmap='gray') # MRI image
        ax[2,idx].axis('off')
        ax[2,idx].tricontour(vox[:, 0], vox[:, 1] , tris, vox[:, 2],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[2,idx].set_aspect('equal')

    # View-4
    for idx,cpos in enumerate(voxposvis4):
        toplot = mridata[:,:,cpos]
        toplot = toplot.swapaxes(0,1)
        ax[3,idx].imshow(toplot, cmap='gray') # MRI image
        ax[3,idx].axis('off')
        ax[3,idx].tricontour(vox[:, 0], vox[:, 1] , tris, vox[:, 2],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[3,idx].set_aspect('equal')

    outname = 'coronal_'+subjname+'.png'
    fig.savefig(join(outpath,'screenshots',outname))

    # Perfom axial screenshots
    voxposvis1 = [50,55,60,65]
    voxposvis2 = [70,75,80,85]
    voxposvis3 = [90,95,100,105]
    voxposvis4 = [110,120,130,140]
    fig, ax = plt.subplots(4, 4, figsize=(30,30))
    fig.set_tight_layout({'pad': 0})
    fig.subplots_adjust(wspace=0)

    # View-1
    for idx,cpos in enumerate(voxposvis1):
        toplot = mridata[:,cpos,:]
        toplot = toplot.swapaxes(0,1)
        ax[0,idx].imshow(toplot, cmap='gray') # MRI image
        ax[0,idx].axis('off')
        ax[0,idx].tricontour(vox[:, 0], vox[:, 2] , tris, vox[:, 1],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[0,idx].set_aspect('equal')

    # View-2
    for idx,cpos in enumerate(voxposvis2):
        toplot = mridata[:,cpos,:]
        toplot = toplot.swapaxes(0,1)
        ax[1,idx].imshow(toplot, cmap='gray') # MRI image
        ax[1,idx].axis('off')
        ax[1,idx].tricontour(vox[:, 0], vox[:, 2] , tris, vox[:, 1],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[1,idx].set_aspect('equal')

    # View-3
    for idx,cpos in enumerate(voxposvis3):
        toplot = mridata[:,cpos,:]
        toplot = toplot.swapaxes(0,1)
        ax[2,idx].imshow(toplot, cmap='gray') # MRI image
        ax[2,idx].axis('off')
        ax[2,idx].tricontour(vox[:, 0], vox[:, 2] , tris, vox[:, 1],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[2,idx].set_aspect('equal')

    # View-4
    for idx,cpos in enumerate(voxposvis4):
        toplot = mridata[:,cpos,:]
        toplot = toplot.swapaxes(0,1)
        ax[3,idx].imshow(toplot, cmap='gray') # MRI image
        ax[3,idx].axis('off')
        ax[3,idx].tricontour(vox[:, 0], vox[:, 2] , tris, vox[:, 1],              # Plot surface contours
                       levels=[cpos], colors='r', linewidths=1.0,
                       zorder=1)
        ax[3,idx].set_aspect('equal')

    outname = 'axial_'+subjname+'.png'
    fig.savefig(join(outpath,'screenshots',outname))

# ---------------------------
# Run Code
# ---------------------------
if __name__ == "__main__":
    options = options_parser()
    qc_freesurfer(options)

