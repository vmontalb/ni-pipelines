#!/usr/bin/env python
# run_stats.py

#
# Script to run stats of Freesurfer cortical maps.
#
# Original Author: Victor Montal
# Date_first: Jan - 25
# Date:


# ---------------------------
# Import Libraries
import  sys
import os
import shutil
from os.path import join
import numpy as np
import argparse
import subprocess
import pdb

import numpy as np
import pandas as pd
import seaborn as sns
import nibabel as nib
import nibabel.freesurfer.mghformat as nibmgh

from niutils.matplotlib_surface_plotting.matplotlib_surface_plotting import plot_surf_santpau

# ---------------------------
# Parser Options
HELPTEXT = """

run_stats.py [dev version]

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat


>> This script will generate multiple statistical tests, and save the required
outputs in a .pdf report file. <<

Outputs:
---------
    0.- mri_glmfit. Vertex-wise General Linear Model (GLM)
    1.- fspalm. Vertex-wise Permutation statistics based on PALM []
    2.- MultipleComparison with Monte Carlo Gaussian Simulations []
    3.- MultipleComparison with MonteCarlo based on data permutations []
    4.- MultipleComparison with Spherical Rotation simulations []
    5.- Plot most-significant vertex scatterplot/group-comparison
    6.- Plot all the MultplipleComparison tests (uncorrected + overlay clusters)
    7.- (optional) Compute Cohen's d effect size maps []
    8.- (optional) Use Robust Regression



REFS:
-----

[1]

[2]

[3]

[4] Greve DN, Fischl B. 2017. https://doi.org/10.1016/j.neuroimage.2017.12.072


"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_glmd      = 'Path to folder were stats will be performed.'
    h_harmon   = 'Flag to consider if the input Y-file is harmonized .'

    parser.add_argument('--glm-dir',    dest = 'glmdir',    action = 'store',       help = h_glmd,      required = True)
    parser.add_argument('--harmon',     dest = 'harmon',   action = 'store_true',   help = h_harmon,    required = False, default = False)

    args = parser.parse_args()
    return args


# ---------------------------
# Subfunctions
def my_print(message, ff):
    """
    print message, then flush stdout
    """
    print(message)
    ff.write(message+'\n')
    sys.stdout.flush()

def run_cmd(cmd,err_msg,ff):
    """
    execute the comand
    """
    my_print('#@# Command: ' + cmd+'\n', ff)
    retcode = subprocess.Popen(cmd,shell=True, executable='/bin/bash').wait()
    if retcode != 0 :
        my_print('ERROR: '+err_msg, ff)
        sys.exit(1)
    my_print('\n', ff)


# ---------------------------
# Main Code
def runStats(options):
    # Checks and predefine inputs
    glmd = join(options.glmdir,'GLMDIR')
    fshome = os.environ['FREESURFER_HOME']
    logfile = join(options.glmdir, 'stats.log')
    log = open(logfile, 'w')

    contrast = join(glmd,'contrasts')
    if os.path.isfile(contrast):
        pass
    else:
        my_print('Error! No contrast file found at:'+glmd, log )
        sys.exit(1)

    # Define y-file name
    if options.harmon:
        yfile = "_harmon_y.mgh"
    else:
        yfile = "_y.mgh"


#        #___________________Run GLM ___________________
    my_print('> Computing GLM stats at path: '+glmd, log)
    for hemi in ['lh', 'rh']:
        cy = join(glmd,hemi+yfile)
        cout = join(glmd,hemi+'_results')
        cframemask = join(glmd,hemi+'.framemask.mgh')
        cmd = 'mri_glmfit   --glmdir '+cout+' \
                            --y '+cy+' \
                            --X '+join(glmd,'X.mat')+' \
                            --C '+contrast+' \
                            --surf fsaverage '+hemi+' \
                            --cortex --no-prune \
                            --eres-save \
                            --frame-mask '+cframemask
        run_cmd(cmd, 'Computing GLM stats for hemisphere: '+hemi, log)

#        #___________________ FSPALM permutations___________________
    # Check if palm is installed correctly, if not, skip
    my_print('> Computing permutation-based statistics using PALM', log)
    my_print('(!) WARNING. It is possible that due to zero-values in your input data, the results might not be relaiable', log)
    my_print('>     Preparing command to run..', log)
    for hemi in ['lh', 'rh']:
        cout = join(glmd,hemi+'_results')
        cmd = 'fspalm --glmdir '+cout+' \
                        --cft 1.3 --cwp 0.05 \
                        --name palm-permut \
                        --iters 1000 --2spaces \
                        --twotail --monly \
                        --pargs="-logp -T -tfce2D -savemask"'
        run_cmd(cmd,'Woha! Failed to create script to run PALM permutations. Check it! Exiting..', log )

    my_print('>     Running PALM stats..', log)
    for hemi in ['lh', 'rh']:
        cout = join(glmd,hemi+'_results','palm-permut')
        standardp = join(fshome, 'subjects', 'fsaverage', 'surf')
        cmd = "palm -i "+join(glmd, hemi+yfile)+" \
                    -s "+join(standardp,hemi+".white")+" "+join(standardp,hemi+".white.avg.area.mgh")+" \
                    -d "+join(cout,"design.mat")+" \
                    -t "+join(cout,"design.con")+" \
                    -n 1000 \
                    -T -tfce2D -logp -savemask \
                    -twotail \
                    -o "+join(cout,hemi+".palm")

        run_cmd(cmd,'Woha! Failed to run PALM permutations with octave. Check it! Exiting..', log)

#        #___________________MonteCarlo based on Gaussian simulation___________________
    if not (os.path.isfile(join(glmd,'lh_results','fwhm.dat')) and os.path.isfile(join(glmd,'rh_results','fwhm.dat'))):
        my_print("Woha! Something with GLM (mri_glmfit) went wrong. Check it. Exiting..", log)
        sys.exit(1)
    my_print('> Computing Multiple Comparisons tests based on MonteCarlo simulations', log)
    for cthresh in ['13','20']:
        for hemi in ['lh', 'rh']:
            cout = join(glmd,hemi+'_results')
            cfwhm = np.loadtxt(join(cout,'fwhm.dat'))
            cfwhm = np.int(np.ceil(cfwhm))
            csimpath = join(fshome,'average/mult-comp-cor/fsaverage',hemi,'cortex/fwhm'+str(cfwhm),'abs/th'+cthresh,'mc-z.csd')
            cmd = 'mri_surfcluster --in '+join(cout,'contrasts','sig.mgh')+' \
                                    --csd '+csimpath+' \
                                    --mask '+join(cout,'mask.mgh')+' \
                                    --cwsig '+join(cout,'contrasts','mc-z.abs.th'+cthresh+'.sig.cluster.mgh')+' \
                                    --vwsig '+join(cout,'contrasts','abs.th'+cthresh+'.sig.vertex.mgh')+' \
                                    --sum '+join(cout,'contrasts','mc-z.abs.th'+cthresh+'.sig.cluster.summary')+' \
                                    --ocn '+join(cout,'contrasts','mc-z.abs.th'+cthresh+'.sig.ocn.mgh')+' \
                                    --oannot '+join(cout,'contrasts','mc-z.abs.th'+cthresh+'.sig.ocn.annot')+' \
                                    --csdpdf '+join(cout,'contrasts','mc-z.abs.th'+cthresh+'.pdf.dat')+' \
                                    --annot aparc \
                                    --cwpvalthresh 0.05 \
                                    --surf white'
            run_cmd(cmd,'Woha! MontCarlo based on Gaussian simmulations went wrong. Check it! Exiting..', log)


#        #___________________ MonteCarlo based on permutation___________________
    if not (os.path.isfile(join(glmd,'lh_results','fwhm.dat')) and os.path.isfile(join(glmd,'rh_results','fwhm.dat'))):
        my_print("Woha! Something with GLM (mri_glmfit) went wrong. Check it. Exiting..", log)
        sys.exit(1)
    my_print('> Computing Multiple Comparisons tests based on permutations', log)
    for hemi in ['lh', 'rh']:
        cout = join(glmd,hemi+'_results')
        cmd = 'mri_glmfit-sim   --glmdir '+cout+' \
                                --perm 1000 1.3 abs \
                                --cwp 0.05 \
                                --2spaces \
                                --bg 1'
        run_cmd(cmd,'Woha! MontCarlo based on permutations went wrong. Check it! Exiting..', log)

#        #___________________SPIN spherical rotation___________________



#        #___________________Screenshot surface results___________________
    my_print('> Performing results screenshots', log)
    outss = join(glmd,'summary-results')
    try:
        os.makedirs(outss)
    except:
        shutil.rmtree(outss)
        os.makedirs(outss)
    standardp = join(fshome, 'subjects', 'fsaverage')
    ctmp = join(outss,'tmp')
    os.makedirs(ctmp)

    my_print('>     GLM uncorrected maps', log)
    for hemi in ['lh', 'rh']:
        cin = join(glmd,hemi+'_results','contrasts','sig.mgh')
        csulc = join(standardp,'surf',hemi+".sulc")
        ctemplate = join(standardp,'surf',hemi+".inflated")
        ccortex = join(standardp,'label',hemi+".cortex.label")
        cout = join(ctmp,hemi+".glm-uncorr-sig.png")
        # Load data
        vertices, faces = nib.freesurfer.io.read_geometry(ctemplate)
        sulc = nib.freesurfer.io.read_morph_data(csulc)
        overlay = nibmgh.load(cin).get_fdata()
        overlay = overlay.reshape(overlay.shape[0], overlay.shape[1]*overlay.shape[2])
        overlay = np.array(overlay[:,0])
        cortex = nib.freesurfer.io.read_label(ccortex)

        # Plot
        if hemi == 'lh':
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[90,270], filename=cout,
                                vmax = 5, vmin=1.3, title = 'Uncorrected p-value',
                                cmap_label='p-value', colorbar = True)
        else:
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[270,90], filename=cout,
                                vmax = 5, vmin=1.3,
                                cmap_label='',colorbar = True)
    #append images
    cmd = "convert -limit memory 1000MiB -append -append \
                    "+join(ctmp,'lh.glm-uncorr-sig.png')+" \
                    "+join(ctmp,'rh.glm-uncorr-sig.png')+" \
                    "+join(ctmp,'glm-uncorr-sig.png')
    run_cmd(cmd,'Woha! Error merging GLM uncorrected images -.-', log)

    my_print('>     GLM MonteCarlo-Sim maps', log)
    for hemi in ['lh', 'rh']:
        cin = join(glmd,hemi+'_results','contrasts','sig.mgh')
        cmcz = join(glmd,hemi+'_results','contrasts','mc-z.abs.th13.sig.cluster.mgh')
        csulc = join(standardp,'surf',hemi+".sulc")
        ctemplate = join(standardp,'surf',hemi+".inflated")
        ccortex = join(standardp,'label',hemi+".cortex.label")
        cout = join(ctmp,hemi+".glm-MonteCarlo-sim-sig.png")
        # Load data
        vertices, faces = nib.freesurfer.io.read_geometry(ctemplate)
        sulc = nib.freesurfer.io.read_morph_data(csulc)
        overlay = nibmgh.load(cin).get_fdata()
        overlay = overlay.reshape(overlay.shape[0], overlay.shape[1]*overlay.shape[2])
        overlay = np.array(overlay[:,0])
        mczmask = nibmgh.load(cmcz).get_fdata()[:,:,:,0]
        mczmask = mczmask.reshape(mczmask.shape[0], mczmask.shape[1]*mczmask.shape[2])
        mczmask = np.array(mczmask[:,0])
        cortex = nib.freesurfer.io.read_label(ccortex)

        # Plot
        if hemi == 'lh':
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[90,270], filename=cout,
                                vmax = 5, vmin=1.3, mask=mczmask,title = 'MonteCarlo-Sim corrected p-value',
                                cmap_label='p-value', colorbar = True)
        else:
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[270,90], filename=cout,
                                vmax = 5, vmin=1.3, mask=mczmask,
                                cmap_label='', colorbar = True)
    #append images
    cmd = "convert -limit memory 1000MiB -append -append \
                    "+join(ctmp,'lh.glm-MonteCarlo-sim-sig.png')+" \
                    "+join(ctmp,'rh.glm-MonteCarlo-sim-sig.png')+" \
                    "+join(ctmp,'glm-MonteCarlo-sim-sig.png')
    run_cmd(cmd,'Woha! Error merging MonteCarlo-simulated images -.-', log)

    my_print('>     GLM MonteCarlo-Permutation maps', log)
    for hemi in ['lh', 'rh']:
        cin = join(glmd,hemi+'_results','contrasts','sig.mgh')
        cmcz = join(glmd,hemi+'_results','contrasts','perm.th13.abs.sig.cluster.mgh')
        csulc = join(standardp,'surf',hemi+".sulc")
        ctemplate = join(standardp,'surf',hemi+".inflated")
        ccortex = join(standardp,'label',hemi+".cortex.label")
        cout = join(ctmp,hemi+".glm-MonteCarlo-permut-sig.png")
        # Load data
        vertices, faces = nib.freesurfer.io.read_geometry(ctemplate)
        sulc = nib.freesurfer.io.read_morph_data(csulc)
        overlay = nibmgh.load(cin).get_fdata()
        overlay = overlay.reshape(overlay.shape[0], overlay.shape[1]*overlay.shape[2])
        overlay = np.array(overlay[:,0])
        mczmask = nibmgh.load(cmcz).get_fdata()
        mczmask = mczmask.reshape(mczmask.shape[0], mczmask.shape[1]*mczmask.shape[2])
        mczmask = np.array(mczmask[:,0])
        cortex = nib.freesurfer.io.read_label(ccortex)

        # Plot
        if hemi == 'lh':
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[90,270], filename=cout,
                                vmax = 5, vmin=1.3, mask=mczmask,title = 'MonteCarlo-Permutation corrected p-value',
                                cmap_label='p-value', colorbar = True)
        else:
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[270,90], filename=cout,
                                vmax = 5, vmin=1.3, mask=mczmask,
                                cmap_label='', colorbar = True)
    #append images
    cmd = "convert -limit memory 1000MiB -append -append \
                    "+join(ctmp,'lh.glm-MonteCarlo-permut-sig.png')+" \
                    "+join(ctmp,'rh.glm-MonteCarlo-permut-sig.png')+" \
                    "+join(ctmp,'glm-MonteCarlo-permut-sig.png')
    run_cmd(cmd,'Woha! Error merging MonteCarlo permut images -.-', log)

    my_print('>     PALM Permutation maps', log)
    for hemi in ['lh', 'rh']:
        cin = join(glmd,hemi+'_results','palm-permut',hemi+'.palm_tfce_tstat_fwep.mgz')
        csulc = join(standardp,'surf',hemi+".sulc")
        ctemplate = join(standardp,'surf',hemi+".inflated")
        ccortex = join(standardp,'label',hemi+".cortex.label")
        cout = join(ctmp,hemi+".PALM-permut-sig.png")
        # Load data
        vertices, faces = nib.freesurfer.io.read_geometry(ctemplate)
        sulc = nib.freesurfer.io.read_morph_data(csulc)
        overlay = nibmgh.load(cin).get_fdata()
        overlay = overlay.reshape(overlay.shape[0], overlay.shape[1]*overlay.shape[2])
        overlay = np.array(overlay[:,0])
        cortex = nib.freesurfer.io.read_label(ccortex)

        # Plot
        if hemi == 'lh':
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[90,270], filename=cout,
                                vmax = 5, vmin=1.3,title = 'PALM permutations p-value',
                                cmap_label='p-value', colorbar = True)
        else:
            plot_surf_santpau(vertices=vertices, faces=faces, sulc=sulc, overlay=overlay,
                                rotate=[270,90], filename=cout,
                                vmax = 5, vmin=1.3,
                                cmap_label='', colorbar = True)
    #append images
    cmd = "convert -limit memory 1000MiB -append \
                    "+join(ctmp,'lh.PALM-permut-sig.png')+" \
                    "+join(ctmp,'rh.PALM-permut-sig.png')+" \
                    "+join(ctmp,'PALM-permut-sig.png')
    run_cmd(cmd,'Woha! Error merging PALM permut images -.-', log)

#        #___________________Scatterplot most significant vertex____________
    my_print('>     Scatterplots', log)
    for hemi in ['lh', 'rh']:
        cinsig = join(glmd,hemi+'_results','contrasts','sig.mgh')
        cindat = join(glmd,hemi+yfile)
        cinmask = join(glmd,hemi+".framemask.mgh")
        cinglm = join(glmd,'glm.txt')

        # Find max significant vertex
        sig= nibmgh.load(cinsig).get_fdata()
        sig = sig.reshape(sig.shape[0], sig.shape[1]*sig.shape[2])
        cdata = nibmgh.load(cindat).get_fdata()
        cdata = cdata.reshape(cdata.shape[0], cdata.shape[1]*cdata.shape[2]*cdata.shape[3])
        cmask = nibmgh.load(cinmask).get_fdata()
        cmask = cmask.reshape(cmask.shape[0], cmask.shape[1]*cmask.shape[2]*cmask.shape[3])
        cglm = np.loadtxt(cinglm)

        cmax = np.argmax(np.abs(sig))

        # Clean zero values of neuroimaging data to plot
        nonzero = np.where(cmask[cmax,:] == 1)[0]
        cdata = cdata[cmax,nonzero]
        cglm = cglm[nonzero,1]

        # Plot scatter
        if sig[cmax] > 0:
            col = "r"
        else:
            col = "b"
        cindep = cglm
        cdepen = cdata
        toplot = pd.DataFrame({'INDEP' : cindep, 'DEPEN' : cdepen})
        p = snsplot = sns.jointplot(x='INDEP', y='DEPEN', data=toplot,
                                kind='reg', truncate=False,
                                color = col)
        p.fig.suptitle(hemi+" max significant scatter")
        p.fig.subplots_adjust(top=0.90) # Reduce plot to make room
        p.savefig(join(ctmp,hemi+".scatter_maxsig_vertex.png"))
    #append images
    cmd = "convert -limit memory 1000MiB +append \
                    "+join(ctmp,'lh.scatter_maxsig_vertex.png')+" \
                    "+join(ctmp,'rh.scatter_maxsig_vertex.png')+" \
                    -background white -alpha off \
                    "+join(ctmp,'scatter_maxsig_vertex.png')
    run_cmd(cmd,'Woha! Error merging scatterplot images -.-', log)


#        #___________________Summary results into pdf____________
    cmd = 'convert '+join(ctmp,'PALM-permut-sig.png')+' -print "%w" /dev/null'
    size = os.popen(cmd).read()
    cmd = 'convert -size '+size+'x250 xc:white '+join(ctmp,'white.png')
    run_cmd(cmd, "Whoa, error creating blank-white image", log)
    # Merge first page
    cmd = "convert -limit memory 1000MiB -append\
                    "+join(ctmp,'glm-uncorr-sig.png')+" \
                    "+join(ctmp,'white.png')+" \
                    "+join(ctmp,'glm-MonteCarlo-sim-sig.png')+" \
                    -background white -alpha off \
                    "+join(ctmp,'page1.png')
    run_cmd(cmd,'Woha! Error preparing pdf -.-', log)
    # Merge second page
    cmd = "convert -limit memory 1000MiB -append \
                    "+join(ctmp,'glm-MonteCarlo-permut-sig.png')+" \
                    "+join(ctmp,'white.png')+" \
                    "+join(ctmp,'PALM-permut-sig.png')+" \
                    -background white -alpha off \
                    "+join(ctmp,'page2.png')
    run_cmd(cmd,'Woha! Error preparing pdf -.-', log)

    # Convert to pdf
    # install img2pdf
    # conda install -c conda-forge pikepdf
    # pip install img2pdf
    glmname = os.path.basename(options.glmdir)
    cmd = "img2pdf --output "+join(outss,'summary-'+glmname+".pdf")+" \
                    --pagesize A5 \
                    "+join(ctmp,'page1.png')+" \
                    "+join(ctmp,'page2.png')+" \
                    "+join(ctmp,'scatter_maxsig_vertex.png')
    run_cmd(cmd,'Woha! Error preparing pdf -.-', log)
    # Delete temporal files
    shutil.rmtree(ctmp)
# ---------------------------
# Run
if __name__ == "__main__":
    options = options_parser()
    runStats(options)

