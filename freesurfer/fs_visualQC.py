#!/usr/bin/env python
# fs_visualQC.py
# -*- coding: utf-8 -*-
# Advanced zoom example. Like in Google Maps.
# It zooms only a tile, but not the whole image. So the zoomed tile occupies
# constant memory and not crams it with a huge resized image for the large zooms.

# Part of the code obtained from:
# https://stackoverflow.com/questions/41656176/tkinter-canvas-zoom-move-pan/48137257#48137257

import os
import random
import json
import argparse

import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk

import pdb

# ---------------------------
# Parser Options
HELPTEXT = """

fs_visualQC.py [dev version]

Author:
------
Victor Montal
vmontalb [at] santpau [dot] cat


>> This script help to easly and quickly perform Freesurfer visualQC. It is intended to be runned on SAMWISE umcore data <<

"""

USAGE = """

"""

def options_parser():
    """
    Command Line Options Parser:
    initiate the option parser and return the parsed object
    """
    parser = argparse.ArgumentParser(description = HELPTEXT,usage = HELPTEXT)

    # help text
    h_list = 'Path to list containing all the Freesurfers path.'

    parser.add_argument('--list-fs',    dest = 'subjlist',    action = 'store', help = h_list,  required = True)

    args = parser.parse_args()
    return args



# Define CANVAS zoom, scroll
class AutoScrollbar(ttk.Scrollbar):
    ''' A scrollbar that hides itself if it's not needed.
        Works only if you use the grid geometry manager '''
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            self.grid_remove()
        else:
            self.grid()
            ttk.Scrollbar.set(self, lo, hi)

    def pack(self, **kw):
        raise tk.TclError('Cannot use pack with this widget')

    def place(self, **kw):
        raise tk.TclError('Cannot use place with this widget')

class Zoom_Advanced(ttk.Frame):
    ''' Advanced zoom of the image '''
    def __init__(self, mainframe, path):
        ''' Initialize the main Frame '''
        ttk.Frame.__init__(self, master=mainframe)
        # Vertical and horizontal scrollbars for canvas
        vbar = AutoScrollbar(self.master, orient='vertical')
        hbar = AutoScrollbar(self.master, orient='horizontal')
        vbar.grid(row=0, column=1, sticky='ns')
        hbar.grid(row=1, column=0, sticky='we')
        # Create canvas and put image on it
        self.canvas = tk.Canvas(self.master, highlightthickness=0,
                                xscrollcommand=hbar.set, yscrollcommand=vbar.set)
        self.canvas.grid(row=0, column=0, sticky='nswe')
        self.canvas.update()  # wait till canvas is created
        vbar.configure(command=self.scroll_y)  # bind scrollbars to the canvas
        hbar.configure(command=self.scroll_x)
        # Make the canvas expandable
        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)
        # Bind events to the Canvas
        self.canvas.bind('<Configure>', self.show_image)  # canvas is resized
        self.canvas.bind('<ButtonPress-1>', self.move_from)
        self.canvas.bind('<B1-Motion>',     self.move_to)
        self.canvas.bind('<MouseWheel>', self.wheel)  # with Windows and MacOS, but not Linux
        self.canvas.bind('<Button-5>',   self.wheel)  # only with Linux, wheel scroll down
        self.canvas.bind('<Button-4>',   self.wheel)  # only with Linux, wheel scroll up
        self.image = Image.open(path)  # open image
        self.width, self.height = self.image.size
        self.imscale = 1.0  # scale for the canvaas image
        self.delta = 1.3  # zoom magnitude
        # Put image into container rectangle and use it to set proper coordinates to the image
        self.container = self.canvas.create_rectangle(0, 0, self.width, self.height, width=0)
        self.show_image()

    def scroll_y(self, *args, **kwargs):
        ''' Scroll canvas vertically and redraw the image '''
        self.canvas.yview(*args, **kwargs)  # scroll vertically
        self.show_image()  # redraw the image

    def scroll_x(self, *args, **kwargs):
        ''' Scroll canvas horizontally and redraw the image '''
        self.canvas.xview(*args, **kwargs)  # scroll horizontally
        self.show_image()  # redraw the image

    def move_from(self, event):
        ''' Remember previous coordinates for scrolling with the mouse '''
        self.canvas.scan_mark(event.x, event.y)

    def move_to(self, event):
        ''' Drag (move) canvas to the new position '''
        self.canvas.scan_dragto(event.x, event.y, gain=1)
        self.show_image()  # redraw the image

    def wheel(self, event):
        ''' Zoom with mouse wheel '''
        x = self.canvas.canvasx(event.x)
        y = self.canvas.canvasy(event.y)
        bbox = self.canvas.bbox(self.container)  # get image area
        if bbox[0] < x < bbox[2] and bbox[1] < y < bbox[3]: pass  # Ok! Inside the image
        else: return  # zoom only inside image area
        scale = 1.0
        # Respond to Linux (event.num) or Windows (event.delta) wheel event
        if event.num == 5 or event.delta == -120:  # scroll down
            i = min(self.width, self.height)
            if int(i * self.imscale) < 30: return  # image is less than 30 pixels
            self.imscale /= self.delta
            scale        /= self.delta
        if event.num == 4 or event.delta == 120:  # scroll up
            i = min(self.canvas.winfo_width(), self.canvas.winfo_height())
            if i < self.imscale: return  # 1 pixel is bigger than the visible area
            self.imscale *= self.delta
            scale        *= self.delta
        self.canvas.scale('all', x, y, scale, scale)  # rescale all canvas objects
        self.show_image()

    def show_image(self, event=None):
        ''' Show image on the Canvas '''
        bbox1 = self.canvas.bbox(self.container)  # get image area
        # Remove 1 pixel shift at the sides of the bbox1
        bbox1 = (bbox1[0] + 1, bbox1[1] + 1, bbox1[2] - 1, bbox1[3] - 1)
        bbox2 = (self.canvas.canvasx(0),  # get visible area of the canvas
                 self.canvas.canvasy(0),
                 self.canvas.canvasx(self.canvas.winfo_width()),
                 self.canvas.canvasy(self.canvas.winfo_height()))
        bbox = [min(bbox1[0], bbox2[0]), min(bbox1[1], bbox2[1]),  # get scroll region box
                max(bbox1[2], bbox2[2]), max(bbox1[3], bbox2[3])]
        if bbox[0] == bbox2[0] and bbox[2] == bbox2[2]:  # whole image in the visible area
            bbox[0] = bbox1[0]
            bbox[2] = bbox1[2]
        if bbox[1] == bbox2[1] and bbox[3] == bbox2[3]:  # whole image in the visible area
            bbox[1] = bbox1[1]
            bbox[3] = bbox1[3]
        self.canvas.configure(scrollregion=bbox)  # set scroll region
        x1 = max(bbox2[0] - bbox1[0], 0)  # get coordinates (x1,y1,x2,y2) of the image tile
        y1 = max(bbox2[1] - bbox1[1], 0)
        x2 = min(bbox2[2], bbox1[2]) - bbox1[0]
        y2 = min(bbox2[3], bbox1[3]) - bbox1[1]
        if int(x2 - x1) > 0 and int(y2 - y1) > 0:  # show image if it in the visible area
            x = min(int(x2 / self.imscale), self.width)   # sometimes it is larger on 1 pixel...
            y = min(int(y2 / self.imscale), self.height)  # ...and sometimes not
            image = self.image.crop((int(x1 / self.imscale), int(y1 / self.imscale), x, y))
            imagetk = ImageTk.PhotoImage(image.resize((int(x2 - x1), int(y2 - y1))))
            imageid = self.canvas.create_image(max(bbox2[0], bbox1[0]), max(bbox2[1], bbox1[1]),
                                               anchor='nw', image=imagetk)
            self.canvas.lower(imageid)  # set image into background
            self.canvas.imagetk = imagetk  # keep an extra reference to prevent garbage-collection

## - Parse image list (and check exist and permissions)
# Get image list
options = options_parser()
#path = '/media/neuroimagen2/Nuevo_vol/COPIA_DE_SEGURIDAD_HDD/ANALISIS/TOOLS/qatools-python/testing/freesurfer_sub-002S0413_ses-m132_v7.1.0'  # place path to your image here
with open(options.subjlist) as f:
    subjlist = f.readlines()
subjlist = [x.strip() for x in subjlist]

# Loop over images without having to rewrite every item the whole time
# https://stackoverflow.com/questions/34527490/how-do-i-get-my-tkinter-picture-viewer-working


## - Functions for looping
# -------------------------
# Next image
def forward():
    global subjlist
    global cidx
    global loadjson
    global freesurferversion
    cidx += 1
    print(cidx)
    csub = os.path.basename(subjlist[cidx])
    sspath = os.path.join(subjlist[cidx],'QC','screenshots')
    currss = os.path.join(sspath,'coronal_'+csub+'.png')
    jsonfile = os.path.join(os.path.dirname(subjlist[cidx]),freesurferversion+'.json')

    # Update json
    [loadjson,jsontext] = parseJson(jsonfile)
    jsonlab['text'] = jsontext

    # Update image
    app = Zoom_Advanced(root, path=currss)

    # Update subject path text
    text_file['text'] = subjlist[cidx]

    # Enable again Save botton
    button_save['state'] = 'normal'

    # Check if its last image to check
    button_previous['state'] = 'normal'
    if cidx == (len(subjlist) - 1):
        button_next['state'] = 'disabled'
    else:
        button_next['state'] = 'normal'

# Previous image
def previous():
    global subjlist
    global cidx
    global loadjson

    cidx -= 1
    print(cidx)
    csub = os.path.basename(subjlist[cidx])
    sspath = os.path.join(subjlist[cidx],'QC','screenshots')
    currss = os.path.join(sspath,'coronal_'+csub+'.png')
    jsonfile = os.path.join(os.path.dirname(subjlist[cidx]),freesurferversion+'.json')

    # Update json
    [loadjson,jsontext] = parseJson(jsonfile)
    jsonlab['text'] = jsontext

    # Update image
    app = Zoom_Advanced(root, path=currss)

    # Update subject path text
    text_file['text'] = subjlist[cidx]

    # Enable again Save botton
    button_save['state'] = 'normal'

    # Check if its last image to check
    button_next['state'] = 'normal'
    if cidx == 0:
        button_previous['state'] = 'disabled'
    else:
        button_previous['state'] = 'normal'


## - Extra functions
# -------------------------
# Get username
def getUsername():
    print(rater.get())
    rater.get()
    pop.destroy()

    # Update buttons
    button_next['state'] = 'normal'
    button_previous['state'] = 'disabled'
    button_axial['state'] = 'normal'
    button_sagittal['state'] = 'normal'
    button_coronal['state'] = 'normal'
    button_save['state'] = 'normal'

    # Add username info bottom page
    text_usernamec = tk.Label(frame_text, text='Username:')
    text_username = tk.Label(frame_text, text=rater.get())
    text_usernamec.grid(row=3, column=0, sticky="W")
    text_username.grid(row=4, column=0, sticky="W")


# Axial view
def axialView():
    global subjlist
    global cidx
    sspath = os.path.join(subjlist[cidx],'QC','screenshots')
    csub = os.path.basename(subjlist[cidx])
    currss = os.path.join(sspath,'axial_'+csub+'.png')
    app = Zoom_Advanced(root, path=currss)

# sagittal view
def sagittalView():
    global subjlist
    global cidx
    sspath = os.path.join(subjlist[cidx],'QC','screenshots')
    csub = os.path.basename(subjlist[cidx])
    currss = os.path.join(sspath,'sagittal_'+csub+'.png')
    app = Zoom_Advanced(root, path=currss)

# coronal view
def coronalView():
    global subjlist
    global cidx
    sspath = os.path.join(subjlist[cidx],'QC','screenshots')
    csub = os.path.basename(subjlist[cidx])
    currss = os.path.join(sspath,'coronal_'+csub+'.png')
    app = Zoom_Advanced(root, path=currss)

# Define alt functions
def parseJson(jsonfile):
    with open(jsonfile) as ff:
        loadjson = json.load(ff)
    toprint = json.dumps(loadjson,  indent=4, separators=(',', ': '))
    return loadjson,toprint

def saveJson():
    global subjlist
    global cidx
    csub = os.path.basename(subjlist[cidx])
    jsonfile = os.path.join(os.path.dirname(subjlist[cidx]),freesurferversion+'.json')
    [loadjson,jsontext] = parseJson(jsonfile)
    if 'VisualQc' in jsontext:
        visualqc = 'VisualQc'
    else:
        visualqc = 'VisualQC'

    if loadjson[visualqc] != '-':
        oldQC = loadjson[visualqc]
        oldrater = loadjson['LastUser'+visualqc]

        if loadjson['History'+visualqc] != '-':
            loadjson['History'+visualqc] = loadjson['History'+visualqc] + " / " + oldQC
            loadjson['History'+visualqc] = loadjson['History'+visualqc] + " / " + oldrater
        else:
            loadjson['History'+visualqc] =  oldQC
            loadjson['History'+visualqc] = oldrater

    # Update fields
    loadjson[visualqc] = str(valqc.get())
    loadjson['LastUser'+visualqc] = rater.get()

    # Save json
    with open(jsonfile,'w') as outjson:
        json.dump(loadjson, outjson, indent = 4)

    # Update json data info
    [loadjson,jsontext] = parseJson(jsonfile)
    jsonlab['text'] = jsontext

    # Unable to do multiple save (minimize errors multiple clicks)
    button_save['state'] = 'disable'


## - Initialize graphic window
# -----------------------------
freesurferversion='freesurfer7.1.1'
# Get image list
cidx = 0    # image index
csub = os.path.basename(subjlist[cidx])
sspath = os.path.join(subjlist[cidx],'QC','screenshots')
currss = os.path.join(sspath,'coronal_'+csub+'.png')
jsonfile = os.path.join(os.path.dirname(subjlist[cidx]),freesurferversion+'.json')

# Define Canvas
root = tk.Tk()
root.geometry("1800x1000")
root.title("Visual QC of Freesurfer Segmentation")

# Create general frame
frame_qc = tk.Frame(root)

# Create frame to display current punctiation (Json info)
frame_json = tk.Frame(frame_qc)
idxlab = tk.Label(frame_json, text='JSON Freesurfer info:', justify='left')
[loadjson,jsontext] = parseJson(jsonfile)
print(jsontext)
jsonlab = tk.Label(frame_json, text=jsontext, justify='left')
idxlab.grid(row=0, column=0, sticky="W")
jsonlab.grid(row=1, column=0, sticky="W")
frame_json.grid(row=0,column=0)

# Create frame to put the three views botton
frame_view = tk.Frame(frame_qc)
button_axial = tk.Button(frame_view,text="Ax", command=axialView)
button_sagittal = tk.Button(frame_view,text="Sa", command=sagittalView)
button_coronal= tk.Button(frame_view,text="Co", command=coronalView)
button_axial.grid(row=0,column=0)
button_sagittal.grid(row=0,column=1)
button_coronal.grid(row=0,column=2)
frame_view.grid(row=2,column=0)

# Create frame to give puntuation of image quality
frame_opt = tk.Frame(frame_qc)
valqc = tk.IntVar()
tk.Radiobutton(frame_opt, text='Excluded - 0', variable=valqc, value=0).pack(anchor='w')
tk.Radiobutton(frame_opt, text='Poor quality - 1', variable=valqc, value=1).pack(anchor='w')
tk.Radiobutton(frame_opt, text='Good quality - 2', variable=valqc, value=2).pack(anchor='w')
tk.Radiobutton(frame_opt, text='Excellent quality - 3', variable=valqc, value=3).pack(anchor='w')
frame_opt.grid(row=3,column=0)

"""
# Create frame to enter comments
frame_comm = tk.Frame(frame_qc)
commlab = tk.Label(frame_comm, text='Comments:').pack(anchor='nw')
comminput = tk.Text(frame_comm, width=20, height=10).pack(anchor='w')
frame_comm.grid(row=4,column=0)
"""
# Save evaluation button
button_save = tk.Button(frame_qc, text='Save Visual QC', width=25, command=lambda: saveJson())
button_save.grid(row=4,column=0, sticky="s")

# Current file name:
frame_text = tk.Frame(root)
text_filec = tk.Label(frame_text, text='Current subject:', justify='left')
text_file = tk.Label(frame_text, text=subjlist[cidx], justify='left')
text_filec.grid(row=1, column=0, sticky="W")
text_file.grid(row=2, column=0, sticky="W")


# Create Buttons of next and forward image
frame_next = tk.Frame(root)
button_previous = tk.Button(frame_next,text="<<", command=lambda: previous())
button_next = tk.Button(frame_next,text=">>", command=lambda: forward())
button_previous.grid(row=0,column=0)
button_next.grid(row=0,column=1)

# Run zoom image (first-run)
app = Zoom_Advanced(root, path=currss)

# Set positioning (fist-run)
frame_qc.grid(row=0, column=2)
app.grid(row=0, column=0, rowspan=2)
frame_text.grid(row=3,column=0)
frame_next.grid(row=3,column=2)

# Make buttons by default disabled till username is accessed
button_next['state'] = 'disabled'
button_previous['state'] = 'disabled'
button_axial['state'] = 'disabled'
button_sagittal['state'] = 'disabled'
button_coronal['state'] = 'disabled'
button_save['state'] = 'disabled'

# Initial popup asking for user name
pop = tk.Toplevel(root)
pop.title("UMCORE username info")
pop.geometry("550x50")
frame_username = tk.Frame(pop)
rater = tk.StringVar(pop)
userlab = tk.Label(pop, text='Username:')
userinput = tk.Entry(pop, textvariable = rater, bd=5, width=25)
userbutton = tk.Button(pop,text="Start!", command=getUsername)
userlab.grid(row=0, column=0)
userinput.grid(row=0, column=1)
userbutton.grid(row=0, column=2)

# Loop application
root.mainloop()

